import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { ArchwizardModule } from 'angular-archwizard';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';

import { NgxSpinnerModule } from 'ngx-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';

import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageAccueilComponent } from './page/page-accueil/page-accueil.component';

@NgModule({
  declarations: [PageAccueilComponent],
  imports: [
    CommonModule,

    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    MatIconModule,
    MatListModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    ArchwizardModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,

    MatNativeDateModule,
    MatAutocompleteModule,
    NgSelectModule,
    MatStepperModule,
    MatRadioModule,
    BrowserAnimationsModule,
    MatCardModule,
    NgxSpinnerModule,
    MatTableModule,
    RouterModule.forChild([
      {
        path: 'Logistic',
        redirectTo: 'Logistic/liste/1',
        pathMatch: 'full',
      },
      {
        path: 'company/liste',
        component: PageAccueilComponent,
      },
    ]),
  ],
  exports: [],
  providers: [DatePipe],
})
export class CompanyRegistrationModule {}
