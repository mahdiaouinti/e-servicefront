import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { jsPDF } from 'jspdf';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { MatStepper } from '@angular/material/stepper';
import { HttpClient } from '@angular/common/http';
import * as data from './../../../../assets/data.json';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatIconRegistry } from '@angular/material/icon';
import * as XLSX from 'xlsx';

import {
  FormControl,
  FormGroup,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Country } from '@angular-material-extensions/select-country';

import { ActivatedRoute, Router } from '@angular/router';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css'],
})
export class NewComponent implements OnInit {
  dateofdeclaration = new FormControl();
  @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Choose File';
  firstFormGroup = this.formBuilder.group({
    typedeclaration: ['', Validators.required],
    officeofeclaration: ['', Validators.required],
    manifest: ['', Validators.required],
    numberofpackages: ['', Validators.required],
    loadlist: ['', Validators.required],
    totalitems: ['', Validators.required],
    bl: ['', Validators.required],
  });
  secondFormGroup = this.formBuilder.group({
    exportercode: ['', Validators.required],
    exporterno: ['', Validators.required],
    exportername: ['', Validators.required],
    consigneecode: ['', Validators.required],
    consigneeno: ['', Validators.required],
    consigneename: ['', Validators.required],
    financialcode: ['', Validators.required],
    financialno: ['', Validators.required],

    financialname: ['', Validators.required],
    declarantcode: ['', Validators.required],
    declarantno: ['', Validators.required],
    declarantname: ['', Validators.required],
  });
<<<<<<< HEAD
  Form3 = this.formBuilder.group({
    exportercode: ['', Validators.required],
  });
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

  onFileChange(event: any) {
    this.spinner.show();
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>event.target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    };
  }
  btn_backround1 = 'rgba(71, 178, 228, 1)';
  btn_backround2 = '#fff';
  color1 = '#fff';
  color2 = '#073866';
  Form1: FormGroup;
  Form2bool: boolean = false;
  passeger: FormGroup | any;
  BlocB1: boolean = true;
  BlocB2: boolean = false;
  BlocB3: boolean = false;
  BlocB4: boolean = false;
  form2Bool: boolean = false;
<<<<<<< HEAD
  message: string | undefined;
=======
  FormVisitPurpose: FormGroup;
  FormCargoSummary: FormGroup;
  FormPassengerSummary: FormGroup;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  option: any;
  national: any[] = [
    { value: 'Foreigner', viewValue: 'Foreigner' },
    { value: 'Libyan', viewValue: 'Libyan' },
    { value: 'Libyan', viewValue: 'Libyan' },
  ];
  blockform_1: any = 'block';
  blockform_2: any = 'none';
  createView() {}
  onStepChange(event: any) {
    console.log(event);
  }
  options: any[] = [
    { name: 'I (IMO No. Used)' },
    { name: 'C  (Call Sign used)' },
  ];

  /*   @ViewChild(MatStepper, { static: true }) myStepper: MatStepper | any;
   */ ngAfterViewInit() {
    // this.myStepper._getIndicatorType = () => 'number';
  }

  constructor(
    private formBuilder: UntypedFormBuilder,
    private httpClient: HttpClient,
    private modalService: NgbModal,

    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router,

    private notifier: NotifierService,
    private spinner: NgxSpinnerService
  ) {
    this.Form1 = this.formBuilder.group({});
  }
  list_ports: any = [];

  ngOnInit(): void {}
  displayFn(object?: any): any | undefined {
    return object ? object.name : undefined;
  }

  closeResult = '';
  open(content: any) {
    this.modalService
      .open(content, {
        centered: true,
        windowClass: 'modal-rounded',
        ariaLabelledBy: 'modal-basic-title',
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason: any) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  Save(content: any) {}

  protected createFromVessel_Call_Infomation(vessel: any): any {
    return {};
  }

  protected createFromVessel_Call_InfomationUpdate(
    vessel: any,
    etat: any
  ): any {
    return {};
  }

  protected createFromVessel(): any {
    return {};
  }

  protected createFromVesselUpdate(id: any): any {
    return {};
  }

  protected createFromCargoSummary(i: any, call: any): any {
    return {};
  }

  protected createFromVisitPurpose(i: any, call: any): any {
    return {};
  }

  protected createPassengerSummary(i: any, call: any): any {
    return {};
  }

  type: any = 'Ship_Arrival';

  protected GetCellInformationById(id: any) {}
  id_vessel: any;
  Create_Form1_update(Cell: any, Vessel: any) {}

  Create_Form2_update(Cell: any) {}

  Create_Form3_update(Cell: any) {}
  Create_Form4_update(Cell: any) {}

  form2 = 'form2';
  form1 = 'form1';
  selectionChange() {}
  Formbool1: boolean = true;
  Formbool2: boolean = false;
  ChangeForm(form: any) {
    if (form == 'form1') {
      this.Formbool1 = true;
      this.Formbool2 = false;
      this.btn_backround1 = 'rgba(71, 178, 228, 1)';
      this.btn_backround2 = '#fff';
      this.color1 = '#fff';
      this.color2 = '#073866';
      this.blockform_1 = 'block';
      this.blockform_2 = 'none';
    } else {
      this.blockform_1 = 'none';
      this.blockform_2 = 'block';
      this.form2Bool = true;
      this.Formbool1 = false;
      this.Formbool2 = true;
      this.btn_backround2 = 'rgba(71, 178, 228, 1)';
      this.btn_backround1 = '#fff';
      this.color2 = '#fff';
      this.color1 = '#073866';
    }
  }

  contour: number = 1;

  Next() {
    this.intialisebloc();
    if (this.contour < 4) {
      this.contour = this.contour + 1;
    }
    this.remlirbloc();
  }

  Preview() {
    if (this.contour > 1) {
      this.contour = this.contour - 1;
    }

    this.intialisebloc();
    this.remlirbloc();
  }
  intialisebloc() {
    this.BlocB1 = false;
    this.BlocB2 = false;
    this.BlocB3 = false;
    this.BlocB4 = false;
  }

  remlirbloc() {
    if (this.contour == 1) {
      this.BlocB1 = true;
    }
    if (this.contour == 2) {
      this.BlocB2 = true;
    }
    if (this.contour == 3) {
      this.BlocB3 = true;
    }
    if (this.contour == 4) {
      this.BlocB4 = true;
    }
  }

  changebloc(i: any) {
    this.contour = i;
    this.intialisebloc();
    this.remlirbloc();
  }

  Go_list() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('Logistic/liste/0');
  }

  Submit(content: any) {}

<<<<<<< HEAD
  code_country: any;
  name_country: any;
  onCountrySelected(country: Country) {
    this.code_country = country.alpha2Code;
    this.name_country = country.name;
  }

  code_country_destination: any;
  name_country_destination: any;
  onCountrySelected_destination(country: Country) {
    this.code_country_destination = country.alpha2Code;
    this.name_country_destination = country.name;
  }
  code_country_origin: any;
  name_country_origin: any;
  onCountrySelected_origin(country: Country) {
    this.code_country_origin = country.alpha2Code;
    this.name_country_origin = country.name;
  }

  code_country_border: any;
  name_country_border: any;
  onCountrySelected_border(country: Country) {
    this.code_country_border = country.alpha2Code;
    this.name_country_border = country.name;
  }

  code_country_Departure: any;
  name_country_Departure: any;
  onCountrySelected_Departure(country: Country) {
    this.code_country_Departure = country.alpha2Code;
    this.name_country_Departure = country.name;
  }

=======
  code_country_passenger: any;
  name_country_passenger: any;
  onCountrySelected_passenger(country: Country) {
    console.log(country.name);
    this.code_country_passenger = country.alpha2Code;
    this.name_country_passenger = country.name;
  }
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  date_ofdeclaration: any;
  public onchangedateofdecalaration(event: any): void {
    this.date_ofdeclaration = this.datePipe.transform(event, 'yyyy-MM-dd');
  }
}
