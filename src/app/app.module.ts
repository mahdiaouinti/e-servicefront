import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeModule } from './Home/home/Home.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';

import { DashboardModule } from './Dashboard/Dashboard.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ElogisticModule } from './E-logistic/E-logistic.module';
import { MatTabsModule } from '@angular/material/tabs';
import { EcustomsModule } from './E-customs/Ecustoms.module';
import { EtradeModule } from './E-trade/Etrade.module';
import { CompanyRegistrationModule } from './Company-registration/Company_registration.module';
import { HeaderComponent } from './layouts/header/header.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './Interceptors/auth.interceptor';
<<<<<<< HEAD
import { UserService } from 'src/Services/User/user.service';
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ElogisticModule,
    EcustomsModule,
    FontAwesomeModule,
    DashboardModule,
    EtradeModule,
    HomeModule,
    NgbModule,
    NgxSpinnerModule,
    CompanyRegistrationModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
