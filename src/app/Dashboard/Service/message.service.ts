import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  url: any;

  Spring_serveur: string = 'api/message/';
  constructor(private router: Router, private http: HttpClient) {
    this.url = environment.SpadesUrl + this.Spring_serveur;
  }

  public post(formData: any) {
    return this.http.post(this.url, formData, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }
}
