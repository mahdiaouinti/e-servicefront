import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VesselCallInformationService } from 'src/app/E-logistic/Shipping_Arrival/Service/VesselCall/vessel-call-information.service';

import { MessageService } from '../Service/message.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  statuslist: any[] = ['For confirmation', 'Reject', 'Accept'];

  constructor(
    private ArrCellInformationApi: VesselCallInformationService,
    private router: Router,
    private modalService: NgbModal,
    private datePipe: DatePipe,
    private MessageApi: MessageService
  ) {}
  itemlist: any[] = ['item1', 'item2'];
  ngOnInit(): void {
    this.GetArrCellInformation();
  }
  CellList: any[] = [];
  type: any;
  GetArrCellInformation() {
    this.CellList = [];
    if (this.index == 0) {
      this.type = 'Ship_Arrival';
    } else {
      this.type = 'Ship_Departure';
    }

    this.ArrCellInformationApi.GetByType(this.type).subscribe((res: any) => {
      this.CellList = res;
      setTimeout(() => {
        this.CellList = this.CellList.filter((e) => e.etat == this.etat);
        console.log(this.CellList);
      }, 200);
    });
  }

  message: string | undefined;

  open(content: any) {
    this.modalService.dismissAll();
    this.modalService.open(content);
  }
  open_message(content: any) {
    this.modalService.dismissAll();
    this.modalService.open(content);
  }
  view_cell(content: any) {
    this.modalService.open(content, {
      size: 'xl',
      windowClass: 'dark-modal',
    });
  }

  dimision() {
    this.modalService.dismissAll();
  }
  logistic_bool: boolean = true;
  trad_bool: boolean = false;
  company_bool: boolean = false;
  customs_bool: boolean = false;

  active: boolean = false;

  onchangedate2(event: any) {}

  onchangedate1(event: any) {}

  changetype(event: any) {}

  selectedport: string | any;

  document: any;
  id_document: any;
  portofcallcode: any;
  voyageno: any;
  callsigntype: any;
  callsign: any;
  vesselbuildyear: any;
  vesseldraft: any;
  vesselregport: any;
  vesselnetweight: any;
  vesselgrossweight: any;
  vesseltype: any;
  vesselcountry: any;
  accept(content: any) {
    this.changeetat('Accept');
    this.modalService.open(content);
  }
  reject() {
    this.changeetat('Reject');
    this.modalService.dismissAll();
  }

  ownercode: any;
  shippingagentcode: any;
  shipmaster: any;
  preferreddock: any;
  shippingagent: any;

  show_document(document: any) {
    this.active = true;
    this.id_document = document.id;
    this.document = document;
    this.voyageno = document.voyageno;
    this.portofcallcode = document.portofcallcode;
    this.callsigntype = document.callsigntype;
    this.callsign = document.callsign;
    this.vesselbuildyear = document.vessel.vesselbuildyear;
    this.vesseldraft = document.vessel.vesseldraft;
    this.vesselregport = document.vessel.vesselregport;
    this.vesselgrossweight = document.vessel.vesselgrossweight;
    this.vesseltype = document.vessel.vesseltype;
    this.vesselnetweight = document.vessel.vesselnetweight;
    this.vesselcountry = document.vessel.vesselcountryname;
    this.shippingagent = document.shippingagent;
    this.ownercode = document.ownercode;
    this.shippingagent = document.shippingagent;
    this.preferreddock = document.preferreddock;
    this.Create_form(document);
  }

  navigate(url: any) {
    this.renitailiser();
    if (url == 1) {
      this.logistic_bool = true;
      this.router.navigateByUrl('/dashboard/logistic');
    }
    if (url == 2) {
      this.customs_bool = true;
      this.router.navigateByUrl('/dashboard/customs');
    }
    if (url == 3) {
      this.trad_bool = true;

      this.router.navigateByUrl('/dashboard/trade');
    }
    if (url == 4) {
      this.company_bool = true;
      this.router.navigateByUrl('/dashboard/company');
    }
  }

  renitailiser() {
    this.logistic_bool = false;
    this.trad_bool = false;
    this.company_bool = false;
    this.customs_bool = false;
  }
  etat_f = 'ACCEPT';

  changeetat(etat: any) {
    if (etat == 'Accept') {
      this.etat_f = 'ACCEPT';
    } else {
      this.etat_f = 'REJECT';
    }
<<<<<<< HEAD
    this.ArrCellInformationApi.update(
=======
    this.ArrCellInformationApi.updatecALL(
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
      this.createFromVessel_Call_InfomationUpdate(etat)
    ).subscribe((res: any) => {
      setTimeout(() => {
        console.log(res);
      }, 500);

      this.Shipping_arrival = res;
    });
  }

  protected createFromVessel_Call_InfomationUpdate(etat: any): any {
    return {
      id: this.id_document,
      etat: etat,
      voyageno: this.document.voyageno,
      ownercode: this.document.ownercode,
      ownername: this.document.ownername,
      callsign: this.document.callsign,
      shippingagentcode: this.document.shippingagentcode,
      shippingagent: this.document.shippingagent,
      shipmaster: this.document.shipmaster,
      preferreddock: this.document.preferreddock,
      berthtype: this.document.berthtype,
      portofcallname: this.document.portofcallname,
      portofcallcode: this.document.portofcallcode,
      nextportname: this.document.nextportname,
      nextportcode: this.document.nextportcode,
      previousportname: this.document.previousportname,
      previousportcode: this.document.previousportcode,
      callsigntype: this.document.callsigntype,
      type: this.document.type,
      eta: this.document.eta,
      etd: this.document.etd,
      dangerousgoods: this.document.dangerousgoods,
      vessel: this.document.vessel,
    };
  }

  applyFilter(filtervalue: any) {
    if (this.CellList.length == 0) {
      this.GetArrCellInformation();
    }
    if (filtervalue != '') {
      this.CellList = this.CellList.filter(
        (cellvessel) =>
          cellvessel.voyageno
            .toLowerCase()
            .indexOf(filtervalue.toLowerCase()) !== -1
      );
    } else {
      this.GetArrCellInformation();
    }
  }

  changedateEta(event: any) {
    this.active = false;
    let eta = this.datePipe.transform(event, 'yyyy-MM-dd');
    setTimeout(() => {
      this.CellList = this.CellList.filter(
        (cellvessel) => cellvessel.eta === eta
      );

      if (event == null) {
        this.GetArrCellInformation();
      }
    }, 400);
  }

  changedateEtd(event: any) {
    this.active = false;
    let etd = this.datePipe.transform(event, 'yyyy-MM-dd');
    setTimeout(() => {
      this.CellList = this.CellList.filter(
        (cellvessel) => cellvessel.etd === etd
      );

      if (event == null) {
        this.GetArrCellInformation();
      }
    }, 400);
  }

  Shipping_arrival: any;

  index: any = 0;
  yourFn(event: any) {
    this.CellList = [];
    if (event.index == 0) {
      this.index = event.index;
      this.GetArrCellInformation();
    }

    if (event.index == 1) {
      this.index = event.index;
      this.GetArrCellInformation();
    }
  }

  selectedstatus: String | any;
  typerecherche: any;
  etat: any = 'For confirmation';
  changestatus(event: any) {
    this.active = false;
    if (event.value != undefined) {
      this.etat = event.value;
      this.GetArrCellInformation();
    } else {
      this.etat = 'For confirmation';
      this.GetArrCellInformation();
    }
  }

  vesselname: any;

  vessellength: any;

  ownername: any;
  eta: any;
  etd: any;
  previousportcode: any;
  previousportname: any;

  portofcallname: any;
  nextportcode: any;
  nextportname: any;

  berthtype: any;
  dangerousgoods: any;
  CargoSummarysList: any[] = [];
  PassengerSummarysList: any[] = [];
  VisitPurposesList: any[] = [];

  Create_form(call: any) {
    let vessel = call.vessel;
    this.vesselname = vessel.vessel;
    this.vesseltype = vessel.vesseltype;
    this.vesselbuildyear = vessel.vesselbuildyear;
    this.vesselregport = vessel.vesselregport;
    this.vesseldraft = vessel.vesseldraft;
    this.vessellength = vessel.vessellength;
    this.vesselgrossweight = vessel.vesselgrossweight;
    this.vesselnetweight = vessel.vesselnetweight;
    this.voyageno = vessel.voyageno;
    this.voyageno = call.voyageno;
    this.callsigntype = call.callsigntype;
    this.callsign = call.callsign;
    this.ownercode = call.ownercode;
    this.ownername = call.ownername;
    this.shippingagentcode = call.shippingagentcode;
    this.shippingagent = call.shippingagent;
    this.shipmaster = call.shipmaster;
    this.eta = call.eta;
    this.etd = call.etd;
    this.previousportcode = call.previousportcode;
    this.previousportname = call.previousportname;
    this.portofcallcode = call.portofcallcode;
    this.portofcallname = call.portofcallname;
    this.nextportcode = call.nextportcode;
    this.nextportname = call.nextportname;
    this.preferreddock = call.preferreddock;
    this.berthtype = call.berthtype;
    this.dangerousgoods = call.dangerousgoods;
    this.total_pass1 = 0;
    this.CargoSummarysList = call.arrVciCargoSummarys;
    this.PassengerSummarysList = call.arrVciPassengerSummarys;
    this.VisitPurposesList = call.arrVciVisitPurposes;
    this.total_pass1 = 0;
    this.total_pass2 = 0;
    for (let passenger of call.arrVciPassengerSummarys) {
      if (passenger.passengernationally == 'Libya') {
        this.total_pass1 = this.total_pass1 + passenger.numberofpassenger;
      } else {
        this.total_pass2 = this.total_pass2 + passenger.numberofpassenger;
      }
    }
  }

  total_pass1: any = 0;
  total_pass2: any = 0;
}
