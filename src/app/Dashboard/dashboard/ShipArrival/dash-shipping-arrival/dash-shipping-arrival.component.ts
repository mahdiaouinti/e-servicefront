import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VesselCallInformationService } from 'src/app/E-logistic/Shipping_Arrival/Service/VesselCall/vessel-call-information.service';

@Component({
  selector: 'app-dash-shipping-arrival',
  templateUrl: './dash-shipping-arrival.component.html',
  styleUrls: ['./dash-shipping-arrival.component.css'],
})
export class DashShippingArrivalComponent implements OnInit {
  constructor(
    private ArrCellInformationApi: VesselCallInformationService,
    private router: Router,
    private modalService: NgbModal,
    private datePipe: DatePipe,
    private route: ActivatedRoute
  ) {}
  itemlist: any[] = ['item1', 'item2'];
  ngOnInit(): void {
    this.GetArrCellInformation();
    localStorage.setItem('Id_user', this.route.snapshot.params['iduser']);
  }
  CellList: any[] = [];

  GetArrCellInformation() {
    this.CellList = [];
    this.ArrCellInformationApi.Get().subscribe((res: any) => {
      this.CellList = res;

      this.CellList = this.CellList.filter((e) => e.etat == 'For confirmation');
    });
  }
  message: string | undefined;

  open(content: any) {
    this.modalService.dismissAll();
    this.modalService.open(content);
  }
  open_message(content: any) {
    this.modalService.dismissAll();
    this.modalService.open(content);
  }

  dimision() {
    this.modalService.dismissAll();
  }
  logistic_bool: boolean = true;
  trad_bool: boolean = false;
  company_bool: boolean = false;
  customs_bool: boolean = false;

  active: boolean = false;

  onchangedate2(event: any) {}

  onchangedate1(event: any) {}

  changetype(event: any) {}

  selectedport: string | any;

  document: any;
  id_document: any;
  portofcallcode: any;
  voyageno: any;
  callsigntype: any;
  callsign: any;
  vesselbuildyear: any;
  vesseldraft: any;
  vesselregport: any;
  vesselnetweight: any;
  vesselgrossweight: any;
  vesseltype: any;
  vesselcountry: any;
  accept(content: any) {
    this.changeetat('Accept');
    this.modalService.open(content);
  }
  reject() {
    this.changeetat('Reject');
    this.modalService.dismissAll();
  }

  ownercode: any;
  shippingagentcode: any;
  shipmaster: any;
  preferreddock: any;
  shippingagent: any;

  show_document(document: any) {
    this.active = true;
    this.id_document = document.id;
    this.document = document;
    this.voyageno = document.voyageno;
    this.portofcallcode = document.portofcallcode;
    this.callsigntype = document.callsigntype;
    this.callsign = document.callsign;
    this.vesselbuildyear = document.vessel.vesselbuildyear;
    this.vesseldraft = document.vessel.vesseldraft;
    this.vesselregport = document.vessel.vesselregport;
    this.vesselgrossweight = document.vessel.vesselgrossweight;
    this.vesseltype = document.vessel.vesseltype;
    this.vesselnetweight = document.vessel.vesselnetweight;
    this.vesselcountry = document.vessel.vesselcountryname;
    this.shippingagent = document.shippingagent;
    this.ownercode = document.ownercode;
    this.shippingagent = document.shippingagent;
    this.preferreddock = document.preferreddock;
  }

  navigate(url: any) {
    this.renitailiser();
    if (url == 1) {
      this.logistic_bool = true;
      this.router.navigateByUrl('/dashboard/logistic');
    }
    if (url == 2) {
      this.customs_bool = true;
      this.router.navigateByUrl('/dashboard/customs');
    }
    if (url == 3) {
      this.trad_bool = true;

      this.router.navigateByUrl('/dashboard/trade');
    }
    if (url == 4) {
      this.company_bool = true;
      this.router.navigateByUrl('/dashboard/company');
    }
  }

  renitailiser() {
    this.logistic_bool = false;
    this.trad_bool = false;
    this.company_bool = false;
    this.customs_bool = false;
  }
  etat_f = 'ACCEPT';

  changeetat(etat: any) {
    if (etat == 'Accept') {
      this.etat_f = 'ACCEPT';
    } else {
      this.etat_f = 'REJECT';
    }
<<<<<<< HEAD
    this.ArrCellInformationApi.update(
=======
    this.ArrCellInformationApi.updatecALL(
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
      this.createFromVessel_Call_InfomationUpdate(etat)
    ).subscribe((res: any) => {
      console.log(res);
      this.Shipping_arrival = res;
    });
  }

  protected createFromVessel_Call_InfomationUpdate(etat: any): any {
    return {
      id: this.id_document,
      etat: etat,
      voyageno: this.document.voyageno,
      ownercode: this.document.ownercode,
      ownername: this.document.ownername,
      callsign: this.document.callsign,
      shippingagentcode: this.document.shippingagentcode,
      shippingagent: this.document.shippingagent,
      shipmaster: this.document.shipmaster,
      preferreddock: this.document.preferreddock,
      berthtype: this.document.berthtype,
      portofcallname: this.document.portofcallname,
      portofcallcode: this.document.portofcallcode,
      nextportname: this.document.nextportname,
      nextportcode: this.document.nextportcode,
      previousportname: this.document.previousportname,
      previousportcode: this.document.previousportcode,
      callsigntype: this.document.callsigntype,
      eta: this.document.eta,
      etd: this.document.etd,
      dangerousgoods: this.document.dangerousgoods,
      vessel: this.document.vessel,
    };
  }

  applyFilter(filtervalue: any) {
    if (this.CellList.length == 0) {
      this.GetArrCellInformation();
    }
    if (filtervalue != '') {
      this.CellList = this.CellList.filter(
        (cellvessel) =>
          cellvessel.voyageno
            .toLowerCase()
            .indexOf(filtervalue.toLowerCase()) !== -1
      );
    } else {
      this.GetArrCellInformation();
    }
  }

  changedateEta(event: any) {
    let eta = this.datePipe.transform(event, 'yyyy-MM-dd');

    this.CellList = this.CellList.filter(
      (cellvessel) => cellvessel.eta === eta
    );
  }

  changedateEtd(event: any) {
    let etd = this.datePipe.transform(event, 'yyyy-MM-dd');

    this.CellList = this.CellList.filter(
      (cellvessel) => cellvessel.etd === etd
    );
  }

  Send() {}

  Shipping_arrival: any;
}
