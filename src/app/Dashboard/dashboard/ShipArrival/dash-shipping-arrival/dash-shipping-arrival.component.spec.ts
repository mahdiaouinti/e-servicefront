import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashShippingArrivalComponent } from './dash-shipping-arrival.component';

describe('DashShippingArrivalComponent', () => {
  let component: DashShippingArrivalComponent;
  let fixture: ComponentFixture<DashShippingArrivalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashShippingArrivalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashShippingArrivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
