import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatListModule } from '@angular/material/list';
import { ArchwizardModule } from 'angular-archwizard';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

import { HomeComponent } from './home.component';

import { layoutsModule } from './../../layouts/header/layouts.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatPaginatorModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatCardModule,
    MatTabsModule,
    MatSidenavModule,
    MatStepperModule,
    MatCheckboxModule,
    MatRadioModule,
<<<<<<< HEAD

=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
    BsDropdownModule,
    MatSelectCountryModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatDividerModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    ArchwizardModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    layoutsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    NgSelectModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ]),
  ],
  exports: [HomeComponent],
  providers: [DatePipe],
})
export class HomeModule {}
