import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { VesselCallInformationModule } from './Shipping_Arrival/VesselCallInformation.module';
import { Shipping_departure_gestionModule } from './Shipping_Departure/Shipping_departure_gestion.module';
import { PageAccueilComponent } from './page/page-accueil/page-accueil.component';

import { ArchwizardModule } from 'angular-archwizard';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';

import { NgxSpinnerModule } from 'ngx-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';

import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DashboardComponent } from './page/dashboard/dashboard.component';
<<<<<<< HEAD
import { AuthGuard } from 'src/Services/Guard/auth.guard';
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

@NgModule({
  declarations: [PageAccueilComponent, DashboardComponent],
  imports: [
    CommonModule,

    VesselCallInformationModule,
    Shipping_departure_gestionModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
<<<<<<< HEAD
=======

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
    MatIconModule,
    MatListModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    ArchwizardModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
<<<<<<< HEAD
=======

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
    MatNativeDateModule,
    MatAutocompleteModule,
    NgSelectModule,
    MatStepperModule,
    MatRadioModule,
    BrowserAnimationsModule,
    MatCardModule,
    NgxSpinnerModule,
    MatTableModule,
    RouterModule.forChild([
      {
<<<<<<< HEAD
        path: 'Logistic/liste/:id',
        component: PageAccueilComponent,
        canActivate: [AuthGuard],
=======
        path: 'Logistic',
        redirectTo: 'Logistic/liste/1',
        pathMatch: 'full',
      },
      {
        path: 'Logistic/liste/:id',
        component: PageAccueilComponent,
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
      },
      {
        path: 'Logistic/liste/:id/:iduser',
        component: PageAccueilComponent,
      },
      {
        path: 'Logistic/dashboard',
        component: DashboardComponent,
      },
    ]),
  ],
  exports: [],
  providers: [DatePipe],
})
export class ElogisticModule {}
