export class CallofInformation {
  id: number | undefined;
  no: String | undefined;
  nom: String | undefined;
  codeofdeclaration: String | undefined;
  eta: String | undefined;
  callsign: String | undefined;
  codeprevious: String | undefined;
  status: String | undefined;

  constructor(
    id: number,
    no: string,
    nom: string,
    codeofdeclaration: string,
    eta: string,
    callsign: string,
    codeprevious: string,
    status: string
  ) {
    this.id = id;
    this.no = no;
    this.nom = nom;
    this.codeofdeclaration = codeofdeclaration;
    this.eta = eta;
    this.callsign = callsign;
    this.codeprevious = codeprevious;
    this.status = status;
  }
}
