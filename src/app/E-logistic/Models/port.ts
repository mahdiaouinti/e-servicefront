export class port {
  id: number | undefined;
  name: String | undefined;
  nom: String | undefined;
  code: String | undefined;

  constructor(id: number, name: string, nom: string, code: string) {
    this.id = id;
    this.name = name;
    this.nom = nom;
    this.code = code;
  }
}
