import { TestBed } from '@angular/core/testing';

import { CargoSummaryService } from './cargo-summary.service';

describe('CargoSummaryService', () => {
  let service: CargoSummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CargoSummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
