import { TestBed } from '@angular/core/testing';

import { VesselCallInformationService } from './vessel-call-information.service';

describe('VesselCallInformationService', () => {
  let service: VesselCallInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VesselCallInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
