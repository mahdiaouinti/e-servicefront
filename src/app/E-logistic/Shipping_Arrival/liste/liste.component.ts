import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, UntypedFormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CargoSummaryService } from '../Service/CargoSummary/cargo-summary.service';
import { PassengerSummaryService } from '../Service/PassengerSummary/passenger-summary.service';
import { VesselCallInformationService } from '../Service/VesselCall/vessel-call-information.service';
import { VisitPurposeService } from '../Service/VisitPurpose/visit-purpose.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { SelectionModel } from '@angular/cdk/collections';
import { jsPDF } from 'jspdf';
import { DatePipe } from '@angular/common';
import { ThisReceiver } from '@angular/compiler';
<<<<<<< HEAD
import { MessageUserService } from 'src/Services/MessagesUser/message-user.service';
import { CallofInformation } from '../../Models/ship_arrival';
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css'],
})
export class ListeComponent implements AfterViewInit {
  index: number = 0;
  displayedColumns: string[] = [
    'select',
    'name',
    'progress',
    'portd',
    'eta',
    'callsign',
    'previous',
    'etat',
    'action',
  ];
  dataSource: MatTableDataSource<any>;
  itemlist: any[] = ['ETA', 'ETD'];
  statuslist: any[] = ['Save', 'For confirmation', 'Reject', 'Accept'];
  itemslist: any[] = ['Call Sign', 'Port'];
  selectedtype: String | any;
  selectedstatus: String | any;
  selectedcondition: String | any;
  CellList: any[] = [];
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  vesselname: any;
  vesseltype: any;
  vesselbuildyear: any;
  vesselregport: any;
  vesseldraft: any;
  vessellength: any;
  vesselgrossweight: any;
  vesselnetweight: any;
  voyageno: any;
  callsigntype: any;
  callsign: any;
  ownercode: any;
  ownername: any;
  shippingagentcode: any;
  shippingagent: any;
  shipmaster: any;
  eta: any;
  etd: any;
  previousportcode: any;
  previousportname: any;
  portofcallcode: any;
  portofcallname: any;
  nextportcode: any;
  nextportname: any;
  preferreddock: any;
  berthtype: any;
  dangerousgoods: any;
<<<<<<< HEAD

  displayedColumnsMessages: string[] = [
    'id',
    'status',
    'startDate',
    'endDate',
    'typeFlux',
    'etatFlux',
    'idDossier',
    'typeMessage',
    'typeScenario',
  ];
  dataSourceMessages: MatTableDataSource<any>;
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  constructor(
    private ArrCellInformationApi: VesselCallInformationService,
    private http: HttpClient,
    private VisitPurposeApi: VisitPurposeService,
    private CargoSummaryApi: CargoSummaryService,
    private PassengerSummaryApi: PassengerSummaryService,
    private modalService: NgbModal,
<<<<<<< HEAD
    private datePipe: DatePipe,
    private MessagesUser: MessageUserService
=======
    private datePipe: DatePipe
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  ) {
    this.dataSource = new MatTableDataSource(this.CellList);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.GetArrCellInformation();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  list_delete: any[] = [];
  daterange = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  onchangedatestart(event: any) {
    this.startdate = this.datePipe.transform(event, 'yyyy-MM-dd');
  }
  startdate: any;
  enddate: any;
  listrecherchr: any;
  onchangedateend(event: any) {
    this.CellList = [];
    this.enddate = this.datePipe.transform(event, 'yyyy-MM-dd');

    if (this.typerecherche == 'ETA') {
      this.listrecherchr = this.CellList_1.filter(
        (e) => e.eta > this.startdate && e.eta < this.enddate
      );

      setTimeout(() => {
        this.dataSource = new MatTableDataSource(this.listrecherchr);
        this.dataSource.paginator = this.paginator;
      }, 100);
    } else if (this.typerecherche == 'ETD') {
      this.listrecherchr = this.CellList_1.filter(
        (e) => e.etd > this.startdate && e.etd < this.enddate
      );

      setTimeout(() => {
        this.dataSource = new MatTableDataSource(this.listrecherchr);
        this.dataSource.paginator = this.paginator;
      }, 100);
    } else {
      this.GetArrCellInformation();
    }
  }
  Arr_remove() {
    this.list_delete = [];
    this.selection.selected.forEach((s) => this.list_delete.push(s));

    for (let cell of this.list_delete) {
      let cel = this.CellList_1.find((x) => x.id == cell.id);

      for (let cargo of cel.arrVciCargoSummarys) {
        this.CargoSummary_Delete(cargo.id);
      }

      for (let passenger of cel.arrVciPassengerSummarys) {
        this.PassengerSummary_Delete(passenger.id);
      }

      for (let visit of cel.arrVciVisitPurposes) {
        this.VisitPurpose_Delete(visit.id);
      }

      setTimeout(() => {
        this.ArrCellInformationApi.delete(cel.id).subscribe((res: any) => {
          setTimeout(() => {
            this.GetArrCellInformation();
          }, 700);

          this.modalService.dismissAll();
        });
      }, 500);
    }
  }
  Arr_view(content: any, id: any) {
    this.ArrCellInformationApi.GetById(id).subscribe((res: any) => {
      this.Create_form(res);
      setTimeout(() => {
        this.modalService.open(content, {
          size: 'xl',
          windowClass: 'dark-modal',
        });
      }, 500);
    });
  }

  open(content: any) {
    this.modalService.open(content);
  }
  Arr_update() {}
  CellList_1: any[] = [];

  Create_form(call: any) {
    let vessel = call.vessel;
    this.vesselname = vessel.vessel;
    this.vesseltype = vessel.vesseltype;
    this.vesselbuildyear = vessel.vesselbuildyear;
    this.vesselregport = vessel.vesselregport;
    this.vesseldraft = vessel.vesseldraft;
    this.vessellength = vessel.vessellength;
    this.vesselgrossweight = vessel.vesselgrossweight;
    this.vesselnetweight = vessel.vesselnetweight;
    this.voyageno = vessel.voyageno;
    this.voyageno = call.voyageno;
    this.callsigntype = call.callsigntype;
    this.callsign = call.callsign;
    this.ownercode = call.ownercode;
    this.ownername = call.ownername;
    this.shippingagentcode = call.shippingagentcode;
    this.shippingagent = call.shippingagent;
    this.shipmaster = call.shipmaster;
    this.eta = call.eta;
    this.etd = call.etd;
    this.previousportcode = call.previousportcode;
    this.previousportname = call.previousportname;
    this.portofcallcode = call.portofcallcode;
    this.portofcallname = call.portofcallname;
    this.nextportcode = call.nextportcode;
    this.nextportname = call.nextportname;
    this.preferreddock = call.preferreddock;
    this.berthtype = call.berthtype;
    this.dangerousgoods = call.dangerousgoods;

    this.CargoSummarysList = call.arrVciCargoSummarys;
    this.PassengerSummarysList = call.arrVciPassengerSummarys;
    this.VisitPurposesList = call.arrVciVisitPurposes;
  }
<<<<<<< HEAD

  messageuser: any;
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  GetArrCellInformation() {
    this.CellList = [];
    this.ArrCellInformationApi.GetByType('Ship_Arrival').subscribe(
      (res: any) => {
<<<<<<< HEAD
        this.CellList = res;
        for (let call of this.CellList) {
          this.MessagesUser.Get(call.id).subscribe((res: any) => {
            this.messageuser = res;

            let status = this.messageuser[0]['status'];

            let call1 = new CallofInformation(
              call.id,
              call.voyageno,
              call.vessel.vessel,
              call.portofcallname,
              call.eta,
              call.callsign,
              call.previousportname,
              status
            );
            this.CellList_1.push(call1);
          });
        }
=======
        this.CellList_1 = res;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

        setTimeout(() => {
          this.dataSource = new MatTableDataSource(this.CellList_1);
          this.dataSource.paginator = this.paginator;
<<<<<<< HEAD
        }, 1000);
=======
        }, 500);
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
      }
    );
  }

  createnewCell(id: any, cell: any, vessel: any) {
    return {
      id: id,
      voyageno: cell.voyageno,
      vessel: vessel,
      port_declaration: cell.portofcallcode,
      eta: cell.eta,
      callsign: cell.callsign,
      port_previous: cell.nextportcode,
      etat: cell.etat,
    };
  }

  Save: any = 'Save';
  ngOnInit(): void {}
  typerecherche: any;
  changestatus(event: any) {
    if (event == undefined) {
      this.GetArrCellInformation();
    }
    this.CellList = [];

    this.listrecherchr = this.CellList_1.filter((e) => e.etat == event);

    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.listrecherchr);
      this.dataSource.paginator = this.paginator;
    }, 100);
  }
  changeconditionitem(event: any) {}
  changetype(event: any) {
    this.typerecherche = event;
  }

  CargoSummarysList: any[] = [];
  PassengerSummarysList: any[] = [];
  VisitPurposesList: any[] = [];
  VisitPurpose_Delete(id: any) {
    this.VisitPurposeApi.delete(id).subscribe((res: any) => {});
  }

  CargoSummary_Delete(id: any) {
    this.CargoSummaryApi.delete(id).subscribe((res: any) => {});
  }
  PassengerSummary_Delete(id: any) {
    this.PassengerSummaryApi.delete(id).subscribe((res: any) => {});
  }

  selection = new SelectionModel<any>(true, []);
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  list_print: any[] = [];
  printdoc() {
    this.list_print = [];
    this.selection.selected.forEach((s) => this.list_print.push(s));

    for (let cell of this.list_print) {
      let cel = this.CellList_1.find((x) => x.id == cell.id);
      this.generate_pdf1(cel);
    }
  }

  dimision() {
    this.modalService.dismissAll();
  }
  x: any = 0;
  y: any = 0;

  generate_pdf1(cell: any) {
    this.total_pass1 = 0;
    this.total_pass2 = 0;
    this.y = 0;
    var doc = new jsPDF();
    var img = new Image();
    img.src = './../../../../assets/logo_dark.png';
    doc.setFont('italic');
    doc.addImage(img, 'png', 150, 5, 50, 20);
    doc.setFontSize(10);
    doc.setTextColor('#46B1E3');
    doc.text('Voyage No :' + cell.voyageno, 5, 10);
    doc.text('Shipping Company :' + '_', 5, 15);
    doc.setFontSize(20);
    doc.text('Shipping Arrival', 75, 15);
    doc.setFontSize(11);
    this.y = 40;
    let x1 = 10;
    let x2 = 60;
    let x3 = 110;
    let x4 = 150;
    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Vessel Call Information', 10, 40);

    doc.line(5, 43, 5, 65);
    doc.line(5, 43, 190, 43);
    doc.line(190, 43, 190, 65);
    doc.line(5, 65, 190, 65);

    doc.setTextColor('#1D3A66');
    this.y = 50;
    doc.setFontSize(9);

    doc.text('Vessel Name : ' + cell.vessel.vessel, x1, this.y);
    doc.text('Vessel Reg Port : ' + cell.vessel.vesselregport, x2, this.y);
    doc.text('Vessel Type : ' + cell.vessel.vesseltype, x3, this.y);
    doc.text('Vessel Build Year : ' + cell.vessel.vesselbuildyear, x4, this.y);

    doc.line(5, 73, 5, 105);
    doc.line(5, 73, 190, 73);
    doc.line(190, 73, 190, 105);
    doc.line(5, 105, 190, 105);
    this.y = 60;
    doc.setFontSize(9);
    doc.text('Vessel Length : ' + cell.vessel.vessellength, x1, this.y);
    doc.text(
      'Vessel Gross weight : ' + cell.vessel.vesselgrossweight,
      x2,
      this.y
    );
    doc.text('Vessel Net Weight : ' + cell.vessel.vesselnetweight, x3, this.y);
    doc.text('Vessel Draft : ' + cell.vessel.vesseldraft, x4, this.y);

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Ship Agent', 10, 70);
    doc.setTextColor('#1D3A66');

    this.y = 80;
    doc.setFontSize(9);
    doc.text('Shipping Agent : ' + cell.shippingagent, x1, this.y);
    doc.text('Shipping Agent Code : ' + cell.shippingagentcode, x2, this.y);
    doc.text('Owner Name : ' + cell.vessel.ownername + ' ', x3, this.y);
    doc.text('Owner Code : ' + cell.ownercode, x4, this.y);

    this.y = 90;
    doc.setFontSize(9);
    doc.text('Ship Master : ' + cell.shipmaster, x1, this.y);
    doc.text('ETA : ' + cell.eta + ' ', x2, this.y);
    doc.text('ETD : ' + cell.etd, x3, this.y);
    doc.text('Berth Type : ' + cell.berthtype, x4, this.y);

    this.y = 100;
    doc.setFontSize(9);
    doc.text('Preferred Dock : ' + cell.preferreddock, x1, this.y);
    doc.text('Country : ' + cell.vessel.vesselcountryname + ' ', x2, this.y);
    doc.text('Call Sign : ' + cell.callsign, x3, this.y);
    doc.text('Call Sign Type : ' + cell.callsigntype, x4, this.y);

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Port', 10, 110);
    doc.setTextColor('#1D3A66');
    doc.line(5, 113, 5, 145);
    doc.line(5, 113, 190, 113);
    doc.line(190, 113, 190, 145);
    doc.line(5, 145, 190, 145);
    this.y = 120;
    doc.setFontSize(9);
    doc.text('Port Call Name : ' + cell.portofcallname, x1, this.y);
    doc.text('Port Call Code : ' + cell.portofcallcode + ' ', x2, this.y);

    this.y = 130;
    doc.setFontSize(9);
    doc.text('Port Previous Name : ' + cell.previousportname, x1, this.y);
    doc.text('Port Previous Code : ' + cell.previousportcode + ' ', x2, this.y);

    this.y = 140;
    doc.setFontSize(9);
    doc.text('Port Next Name : ' + cell.nextportname, x1, this.y);
    doc.text('Port Next Code : ' + cell.nextportcode, x2, this.y);

    doc.setFontSize(10);
    doc.setTextColor('#55EC7A');
    doc.text('Visit Purpose', 10, 150);
    doc.text('Cargo Summary', 10, 190);
    doc.text('Passenger Summary', 10, 230);

    doc.line(5, 155, 5, 185);
    doc.line(5, 155, 190, 155);
    doc.line(190, 155, 190, 185);
    doc.line(5, 185, 190, 185);

    doc.line(5, 195, 5, 225);
    doc.line(5, 195, 190, 195);
    doc.line(190, 195, 190, 225);
    doc.line(5, 225, 190, 225);

    doc.line(5, 235, 5, 280);
    doc.line(5, 235, 190, 235);
    doc.line(190, 235, 190, 280);
    doc.line(5, 280, 190, 280);
    let i = 0;
    let y = 0;
    let f = 0;
    doc.setFontSize(9);
    doc.setTextColor('#1D3A66');
    for (let cell1 of cell.arrVciVisitPurposes) {
      doc.text('reason : ' + cell1.reason + ' ', 15, 160 + i);
      i = i + 7;
    }
    for (let cell2 of cell.arrVciCargoSummarys) {
      doc.text('Cargo Code : ' + cell2.cargocode + ' ', 15, 200 + y);
      doc.text('Cargo Amount : ' + cell2.cargoamount + ' ', 60, 200 + y);
      doc.text('Operation Code : ' + cell2.operationcode + ' ', 100, 200 + y);
      y = y + 7;
    }
    for (let cell3 of cell.arrVciPassengerSummarys) {
      doc.text(
        'Number of Passenger : ' + cell3.numberofpassenger + ' ',
        15,
        250 + f
      );
      doc.text(
        'passengernationally  : ' + cell3.passengernationally + ' ',
        70,
        250 + f
      );

      if (cell3.passengernationally == 'Libya') {
        this.total_pass1 = this.total_pass1 + cell3.numberofpassenger;
      } else {
        this.total_pass2 = this.total_pass2 + cell3.numberofpassenger;
      }
      f = f + 7;
    }

    doc.text('Total Libyan Passenger : ' + this.total_pass1 + ' ', 15, 240);
    doc.text('Total foreigner Passenger  : ' + this.total_pass2 + ' ', 70, 240);

    doc.save('SHipping_Arrival num' + cell.voyageno + '.pdf');
  }

  total_pass1: any = 0;
  total_pass2: any = 0;
<<<<<<< HEAD

  list_message: any;
  Arr_messages(content: any, id: any) {
    this.MessagesUser.Get(id).subscribe((res: any) => {
      this.list_message = res;

      setTimeout(() => {
        this.dataSourceMessages = new MatTableDataSource(this.list_message);
        this.dataSourceMessages.paginator = this.paginator;
        this.modalService.open(content, {
          size: 'xl',
          windowClass: 'dark-modal',
        });
      }, 500);
    });
  }
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
}
