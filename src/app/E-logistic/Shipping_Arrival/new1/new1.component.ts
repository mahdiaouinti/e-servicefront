import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { jsPDF } from 'jspdf';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { MatStepper } from '@angular/material/stepper';
import { HttpClient } from '@angular/common/http';
import * as data from './../../../../assets/data.json';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import * as XLSX from 'xlsx';

import {
  FormControl,
  FormGroup,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Country } from '@angular-material-extensions/select-country';
import { VesselCallInformationService } from '../Service/VesselCall/vessel-call-information.service';
import { PassengerSummaryService } from '../Service/PassengerSummary/passenger-summary.service';
import { CargoSummaryService } from '../Service/CargoSummary/cargo-summary.service';
import { VisitPurposeService } from '../Service/VisitPurpose/visit-purpose.service';
import { VesselService } from '../Service/Vessel/vessel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { CountryService } from '../Service/Country/country.service';
import { NotifierService } from 'angular-notifier';
import { NgxSpinnerService } from 'ngx-spinner';
<<<<<<< HEAD
import { port } from '../../Models/port';
=======
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

@Component({
  selector: 'app-new1',
  templateUrl: './new1.component.html',
  styleUrls: ['./new1.component.css'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class New1Component implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Choose File';
<<<<<<< HEAD

  code_country_passenger: any;
  name_country_passenger: any;
  total_pass1: any = 0;
  total_pass2: any = 0;
  y: any = 0;
=======
  /*  uploadFileEvt(imgFile: any) {
    if (imgFile.target.files && imgFile.target.files[0]) {
      this.fileAttr = '';
      Array.from(imgFile.target.files).forEach((file: any) => {
        this.fileAttr += file.name + ' - ';
      });
      // HTML5 FileReader API
      let reader = new FileReader();
      reader.onload = (e: any) => {
        let image = new Image();
        image.src = e.target.result;
        image.onload = (rs) => {
          let imgBase64Path = e.target.result;
        };
      };
      reader.readAsDataURL(imgFile.target.files[0]);
      // Reset if duplicate image uploaded again
      this.fileInput.nativeElement.value = '';
    } else {
      this.fileAttr = 'Choose File';
    }
  } */

  onFileChange(event: any) {
    this.spinner.show();
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>event.target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      setTimeout(() => {
        this.Form1.controls['VoyageNo'].setValue(ws['C9']['w']);
        this.Form1.controls['VesselName'].setValue(ws['C40']['v']);
        this.Form1.controls['VesselType'].setValue(ws['E40']['w']);
        this.Form1.controls['OwnerCode'].setValue(ws['G40']['w']);
        this.Form1.controls['CallSign'].setValue(ws['C41']['v']);
        this.Form1.controls['PreferredDock'].setValue(ws['E41']['w']);
        this.Form1.controls['ShipMaster'].setValue(ws['G41']['w']);

        this.dateETA.setValue(new Date(ws['E13']['w']));
        this.dateETD.setValue(new Date(ws['G13']['w']));

        ///port
        this.port_next_u = this.portlist.find((x) => x.nom == ws['F30']['v']);
        this.port_perivious_u = this.portlist.find(
          (x) => x.nom == ws['F22']['v']
        );
        this.port_ofcall_u = this.portlist.find((x) => x.nom == ws['D20']['v']);
        this.selectedportNext = this.port_next_u;
        this.selectedportcall = this.port_ofcall_u;
        this.selectedportPrevious = this.port_perivious_u;
        this.PortCallName = this.port_ofcall_u.nom;
        this.PortCallcode = this.port_ofcall_u.code;
        this.PortNextName = this.port_next_u.nom;
        this.PortNextcode = this.port_next_u.code;
        this.PortPreviousName = this.port_perivious_u.nom;
        this.PortPreviouscode = this.port_perivious_u.code;

        this.Form1.controls['VesselLength'].setValue(Number(ws['C18']['v']));
        this.Form1.controls['VesselbuildYear'].setValue(Number(ws['F18']['v']));
        this.Form1.controls['VesselDraft'].setValue(Number(ws['H18']['v']));
        this.Form1.controls['VesselNetWeight'].setValue(Number(ws['F19']['v']));
        this.Form1.controls['VesselGrossWeight'].setValue(
          Number(ws['H19']['v'])
        );
        this.spinner.hide();
      }, 500);
      /* save data */
      const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    };
  }
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  btn_backround1 = 'rgba(71, 178, 228, 1)';
  btn_backround2 = '#fff';
  color1 = '#fff';
  color2 = '#073866';
  Form1: FormGroup;
  Form2bool: boolean = false;
  passeger: FormGroup | any;
  BlocB1: boolean = true;
  BlocB2: boolean = false;
  BlocB3: boolean = false;
  BlocB4: boolean = false;
  form2Bool: boolean = false;
  FormVisitPurpose: FormGroup;
  FormCargoSummary: FormGroup;
  FormPassengerSummary: FormGroup;
<<<<<<< HEAD
  contour: number = 1;
  option: any;
  id_vessel: any;
  form2 = 'form2';
  form1 = 'form1';
  type_f: any;
  CallSignType_f: any = 'I';
  type: any = 'Ship_Arrival';
  Formbool1: boolean = true;
  Formbool2: boolean = false;
  shipping_arrival: any;
  code_country: any;
  name_country: any;
=======
  option: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  national: any[] = [
    { value: 'Foreigner', viewValue: 'Foreigner' },
    { value: 'Libyan', viewValue: 'Libyan' },
    { value: 'Libyan', viewValue: 'Libyan' },
  ];
  blockform_1: any = 'block';
  blockform_2: any = 'none';
  createView() {}
  onStepChange(event: any) {
    console.log(event);
  }
  options: any[] = [
    { name: 'I (IMO No. Used)' },
    { name: 'C  (Call Sign used)' },
  ];

  /*   @ViewChild(MatStepper, { static: true }) myStepper: MatStepper | any;
   */ ngAfterViewInit() {
    // this.myStepper._getIndicatorType = () => 'number';
  }

  CallSignType = new UntypedFormControl();
  selectedportPrevious: any;
  selectedportNext: any;
  selectedportcall: any;
  portlistPrevious = [];
  portlistNext = [];
  VisitPurposelist: any[] = [];
  CargoSummarylist: any[] = [];
  PassengerSummarylist: any[] = [];
  portlist: port[] = [];
  Id_ArrCell: any;
  dateETA = new FormControl();
  dateETD = new FormControl();
  transactiontype: any = 'Submit';
  Id_ArrCell_copy: any;
  date_start: any;
<<<<<<< HEAD
  etat: any;
  port_next_u: any;
  port_perivious_u: any;
  port_ofcall_u: any;
  date_ETA: any;
  date_ETD: any;
  closeResult = '';
  number_e: any = 0;
  number_l: any = 0;
  DangerousGood: any = 'Y';
  PortNextName: any;
  PortNextcode: any;
  PortCallName: any;
  PortCallcode: any;
  PortPreviousName: any;
  PortPreviouscode: any;

=======
  role_autorise_save: boolean = false;
  role_autorise_submit: boolean = false;
  idOrgR="2"
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  constructor(
    private formBuilder: UntypedFormBuilder,
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private VesselCallApi: VesselCallInformationService,
    private VisitPurposeApi: VisitPurposeService,
    private CargoSummaryApi: CargoSummaryService,
    private PassengerSummaryApi: PassengerSummaryService,
    private VesselApi: VesselService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
    private CountryApi: CountryService,
    private notifier: NotifierService,
    private spinner: NgxSpinnerService
  ) {
<<<<<<< HEAD
    this.VesselCallApi.Get().subscribe((res: any) => {});
    this.Id_ArrCell = this.route.snapshot.params['id'];

=======
    this.VesselCallApi.Get().subscribe((res: any) => {
      console.log('ok');
    });
    this.Id_ArrCell = this.route.snapshot.params['id'];

    if (localStorage.getItem('Id_user') == '47') {
      this.role_autorise_save = true;
    }

    if (localStorage.getItem('Id_user') == '48') {
      this.role_autorise_submit = true;
    }

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
    this.date_ETA = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.date_start = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    if (this.Id_ArrCell != undefined) {
      this.transactiontype = 'Update';
      this.form2Bool = true;
      setTimeout(() => {
        this.GetCellInformationById(this.Id_ArrCell);
      }, 300);
    }

    this.Id_ArrCell_copy = this.route.snapshot.params['copy'];
    if (this.Id_ArrCell_copy != undefined) {
      this.transactiontype = 'Copy';
      this.form2Bool = true;
      setTimeout(() => {
        this.GetCellInformationById(this.Id_ArrCell_copy);
      }, 300);
    }

    this.Form1 = this.formBuilder.group({
      VoyageNo: ['', Validators.required],
      VesselName: ['', Validators.required],
      VesselCountry: [''],
      VesselType: [''],
      VesselbuildYear: [''],
      VesselRegPort: [''],
      VesselLength: [''],
      VesselGrossWeight: [''],
      VesselNetWeight: [''],
      VesselDraft: [''],
      OwnerCode: [''],
      OwnerName: [''],
      CallSign: [''],
      ShippingAgentCode: [''],
      ShippingAgent: [''],
      ShipMaster: [''],
      PreviousPortPrevious: [''],
      PortofCall: [''],
      portNext: [''],
      BerthType: [''],
      PreferredDock: [''],
    });
    this.passeger = this.formBuilder.group({
      totla_foreigner: [''],
      totla_passenger: [''],
    });

    this.FormVisitPurpose = this.formBuilder.group({
      reason: ['', Validators.required],
    });
    this.FormCargoSummary = this.formBuilder.group({
      OperationCode: ['', Validators.required],
      CargoCode: ['', Validators.required],
      CargoAmount: ['', Validators.required],
    });
    this.FormPassengerSummary = this.formBuilder.group({
      NumberofPassenger: ['', Validators.required],
      PassengerNationally: ['', Validators.required],
    });
  }
  list_ports: any = [];

  ngOnInit(): void {
    this.Getports();
    this.CallSignType.setValue(this.options[0]);
  }
  displayFn(object?: any): any | undefined {
    return object ? object.name : undefined;
  }

<<<<<<< HEAD
=======
  PortPreviousName: any;
  PortPreviouscode: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  changeportPrevious(event: any) {
    let port = this.portlist.find((x) => x.id == event.id);
    this.PortPreviousName = port?.nom;
    this.PortPreviouscode = port?.code;
  }
<<<<<<< HEAD

=======
  PortCallName: any;
  PortCallcode: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  changeportcall(event: any) {
    let port = this.portlist.find((x) => x.id == event.id);
    this.PortCallName = port?.nom;
    this.PortCallcode = port?.code;
  }
<<<<<<< HEAD

=======
  PortNextName: any;
  PortNextcode: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  changeportNext(event: any) {
    let port = this.portlist.find((x) => x.id == event.id);
    this.PortNextName = port?.nom;
    this.PortNextcode = port?.code;
  }
<<<<<<< HEAD

=======
  DangerousGood: any = 'Y';
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  DangerousGoodsGange(event: any) {
    if (event.value == 'no') {
      this.DangerousGood = 'N';
    } else {
      this.DangerousGood = 'Y';
    }
  }

  Getports() {
    this.httpClient.get('./../../../../assets/data.json').subscribe((data) => {
      this.list_ports = data;
      this.portlist = [];
      for (let i = 0; i < this.list_ports.length; i++) {
        this.portlist.push({
          id: this.list_ports[i].id,
          name:
            this.list_ports[i].name +
            '(Code : ' +
            this.list_ports[i].code +
            ')',
          nom: this.list_ports[i].name,
          code: this.list_ports[i].code,
        });
      }
    });
  }
<<<<<<< HEAD

=======
  closeResult = '';
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  open(content: any) {
    this.modalService
      .open(content, {
        centered: true,
        windowClass: 'modal-rounded',
        ariaLabelledBy: 'modal-basic-title',
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason: any) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  VisitPurpose_add() {
    let reason = this.FormVisitPurpose.controls['reason'].value;
    this.modalService.dismissAll();
    this.VisitPurposelist.push({ reason: reason });
    this.FormVisitPurpose.reset();
  }

  VisitPurpose_remove(reason: any) {
    let result = this.VisitPurposelist.find((x) => x == reason);

    const index: number = this.VisitPurposelist.indexOf(reason);
    if (index !== -1) {
      this.VisitPurposelist.splice(index, 1);
    }
  }

<<<<<<< HEAD
=======
  number_e: any = 0;
  number_l: any = 0;

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  PassengerSummary_add() {
    if (this.name_country_passenger == 'Libya') {
      this.number_l =
        this.number_l +
        this.FormPassengerSummary.controls['NumberofPassenger'].value;
    } else {
      this.number_e =
        this.number_e +
        this.FormPassengerSummary.controls['NumberofPassenger'].value;
    }

    this.passeger.controls['totla_passenger'].setValue(this.number_l);
    this.passeger.controls['totla_foreigner'].setValue(this.number_e);
    let NumberofPassenger =
      this.FormPassengerSummary.controls['NumberofPassenger'].value;
    let PassengerNationally = this.name_country_passenger;
    this.modalService.dismissAll();
    this.PassengerSummarylist.push({
      NumberofPassenger: NumberofPassenger,
      PassengerNationally: PassengerNationally,
    });
    this.FormPassengerSummary.reset();
  }

  PassengerSummary_remove(NumberofPassenger: any) {
    const index: number = this.PassengerSummarylist.indexOf(NumberofPassenger);
    if (index !== -1) {
      this.PassengerSummarylist.splice(index, 1);
    }
  }

  CargoSummary_add() {
    let CargoAmount = this.FormCargoSummary.controls['CargoAmount'].value;
    let CargoCode = this.FormCargoSummary.controls['CargoCode'].value;
    let OperationCode = this.FormCargoSummary.controls['OperationCode'].value;
    this.modalService.dismissAll();
    this.CargoSummarylist.push({
      CargoAmount: CargoAmount,
      CargoCode: CargoCode,
      OperationCode: OperationCode,
    });
    this.FormCargoSummary.reset();
  }

  CargoSummary_remove(reason: any) {
    const index: number = this.CargoSummarylist.indexOf(reason);
    if (index !== -1) {
      this.CargoSummarylist.splice(index, 1);
    }
  }
<<<<<<< HEAD

=======
  code_country: any;
  name_country: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  onCountrySelected(country: Country) {
    this.code_country = country.alpha2Code;
    this.name_country = country.name;
  }

<<<<<<< HEAD
=======
  style(event: any) {
    if (event.target.value > 0) {
    }
  }
  CallSignType_f: any = 'I';
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  Save(content: any, content1: any, content2: any) {
    this.etat = 'Save';

    if (this.Form1.status != 'INVALID') {
      this.Vessel_Call_Infomation_add(content);
    } else {
      this.notifier.notify('error', 'Save Opps !   Form Invalid !');
    }
  }

  Vessel_Call_Infomation_add(content: any) {
    if (this.CallSignType.value.name == 'C  (Call Sign used)') {
      this.CallSignType_f = 'C';
    } else {
      this.CallSignType_f = 'I';
    }
    if (this.transactiontype == 'Copy') {
      this.add_Vessel(content);
    } else if (this.transactiontype == 'Update') {
      this.update_vessel(this.id_vessel, content);
    } else {
      this.add_Vessel(content);
    }
  }
<<<<<<< HEAD

=======
  date_ETA: any;
  date_ETD: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  public onchangedateETA(event: any): void {
    this.date_ETA = this.datePipe.transform(event, 'yyyy-MM-dd');
  }
  public onchangedateETD(event: any): void {
    this.date_ETD = this.datePipe.transform(event, 'yyyy-MM-dd');
  }

  add_VisitPurpose(call: any) {
    for (let i = 0; i < this.VisitPurposelist.length; i++) {
      const VisitPurpose = this.createFromVisitPurpose(i, call);
      this.VisitPurposeApi.post(VisitPurpose).subscribe((res: any) => {});
    }
  }
  add_PassengerSummary(call: any) {
    for (let i = 0; i < this.PassengerSummarylist.length; i++) {
      const PassengerSummary = this.createPassengerSummary(i, call);

      this.PassengerSummaryApi.post(PassengerSummary).subscribe(
        (res: any) => {}
      );
    }
  }
  add_CargoSummary(call: any) {
    for (let i = 0; i < this.CargoSummarylist.length; i++) {
      const CargoSummary = this.createFromCargoSummary(i, call);

      this.CargoSummaryApi.post(CargoSummary).subscribe((res: any) => {});
    }
  }
<<<<<<< HEAD

=======
  shipping_arrival: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  add_Vessel(content: any) {
    this.onchangedateETA(this.dateETA.value);
    this.onchangedateETD(this.dateETD.value);

    const Vessel = this.createFromVessel();
    this.VesselApi.post(Vessel).subscribe((res: any) => {
      this.id_vessel = res.id;
      const call_information = this.createFromVessel_Call_Infomation(res);
<<<<<<< HEAD
      this.VesselCallApi.post(call_information).subscribe((res1: any) => {
=======
      this.VesselCallApi.post(call_information,this.idOrgR).subscribe((res1: any) => {
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
        this.Form1.reset();
        this.Id_ArrCell = res1.id;
        this.shipping_arrival = res1;
        setTimeout(() => {
          this.add_VisitPurpose(res1);
          this.add_CargoSummary(res1);
          this.add_PassengerSummary(res1);

          this.modalService.open(content);
        }, 300);
      });
    });
  }
<<<<<<< HEAD
=======
  etat: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

  update_vessel(id: any, content: any) {
    this.date_ETA = this.datePipe.transform(this.dateETA.value, 'yyyy-MM-dd');
    this.date_ETD = this.datePipe.transform(this.dateETD.value, 'yyyy-MM-dd');
    const Vessel = this.createFromVesselUpdate(id);
    this.VesselApi.update(Vessel, id).subscribe((res: any) => {
      const call_information = this.createFromVessel_Call_InfomationUpdate(
        res,
        this.etat
      );
<<<<<<< HEAD
      this.VesselCallApi.update(call_information).subscribe((res: any) => {
=======
      this.VesselCallApi.updatecALL(call_information).subscribe((res: any) => {
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
        this.modalService.open(content);
      });
    });
  }

<<<<<<< HEAD
=======
  headers = [
    'Vessel_Name',
    'Vessel_Type',
    'Vessel_RegPort',
    'Vessel_BuildYear',
  ];
  headers1 = [
    'Vessel_Length',
    'Vessel_GrossWeight',
    'Vessel_NetWeight',
    'Vessel_Draft',
  ];
  headers2 = ['Owner_Code', 'Owner_Name', 'Call_Sign', 'Shipping_AgentCode'];
  headers3 = ['Ship_Master', 'ETA', 'ETD', 'Berth_Type'];
  headers4 = ['Dangerous_Good', 'Country', 'Call_SignType', 'Shipping_Agent'];
  headers7 = ['Preferred_Dock'];
  headers5 = [
    'Port_CallName',
    'Port_CallCode',
    'Port_NextName',
    'Port_NextCode',
  ];
  headers6 = ['Port_PreviousName', 'Port_PreviousCode'];

  generate_pdf1() {
    let VesselReg = '';
    let OwnerName = '';
    let ShippingAgent = '';
    if (this.Form1.controls['VesselRegPort'].value != undefined) {
      VesselReg = this.Form1.controls['VesselRegPort'].value;
    }

    if (this.Form1.controls['OwnerName'].value != undefined) {
      OwnerName = this.Form1.controls['OwnerName'].value;
    }
    if (this.Form1.controls['ShippingAgent'].value != undefined) {
      ShippingAgent = this.Form1.controls['ShippingAgent'].value;
    }

    let table: any[] = [];
    let table1: any[] = [];
    let table2: any[] = [];
    let table3: any[] = [];
    let table4: any[] = [];
    let table5: any[] = [];
    let table6: any[] = [];
    let table7: any[] = [];

    table.push({
      Vessel_Type: this.Form1.controls['VesselType'].value,
      Vessel_Name: this.Form1.controls['VesselName'].value,
      Vessel_BuildYear: this.Form1.controls['VesselbuildYear'].value + '',
      Vessel_RegPort: VesselReg + '',
    });
    table1.push({
      Vessel_Length: this.Form1.controls['VesselLength'].value + '',
      Vessel_GrossWeight: this.Form1.controls['VesselGrossWeight'].value + '',
      Vessel_NetWeight: this.Form1.controls['VesselNetWeight'].value + '',
      Vessel_Draft: this.Form1.controls['VesselDraft'].value + '',
    });

    table2.push({
      Owner_Code: this.Form1.controls['OwnerCode'].value,
      Owner_Name: OwnerName,
      Call_Sign: this.Form1.controls['CallSign'].value,
      Shipping_AgentCode: this.Form1.controls['ShippingAgentCode'].value,
    });
    table3.push({
      Ship_Master: this.Form1.controls['ShipMaster'].value,
      ETA: this.date_ETA,
      ETD: this.date_ETD,
      Berth_Type: this.Form1.controls['BerthType'].value,
    });
    table7.push({
      Preferred_Dock: this.Form1.controls['PreferredDock'].value,
    });
    table4.push({
      Dangerous_Good: this.DangerousGood + '',
      Country: this.name_country + '',
      Call_SignType: this.CallSignType_f,
      Shipping_Agent: ShippingAgent,
    });
    table5.push({
      Port_CallName: this.PortCallName,
      Port_CallCode: this.PortCallcode,
      Port_NextName: this.PortNextName,
      Port_NextCode: this.PortNextcode,
    });
    table6.push({
      Port_PreviousName: this.PortPreviousName,
      Port_PreviousCode: this.PortPreviouscode,
    });

    var doc = new jsPDF();
    var img = new Image();
    img.src = './../../../../assets/logo_dark.png';

    doc.addImage(img, 'png', 150, 5, 50, 20);

    doc.text('Voyage No :' + this.Form1.controls['VoyageNo'].value, 12, 15);
    /*   doc.text('Vessel', 15, 50);
    doc.text('Port', 20, 230); */
    doc.setFont('Lato-Regular', 'Courier-Bold');
    doc.table(10, 50, table, this.headers, { fontSize: 9 });
    doc.table(10, 80, table1, this.headers1, { fontSize: 8 });
    doc.table(10, 110, table2, this.headers2, { fontSize: 9 });
    doc.table(10, 140, table3, this.headers3, { fontSize: 8 });
    doc.table(10, 170, table4, this.headers4, { fontSize: 8 });
    doc.table(10, 200, table7, this.headers7, { fontSize: 8 });
    doc.table(10, 230, table5, this.headers5, { fontSize: 8 });
    doc.table(10, 260, table6, this.headers6, { fontSize: 8 });

    doc.save('mahdi.pdf'); //Download the rendered PDF.
  }
  type_f: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  protected createFromVessel_Call_Infomation(vessel: any): any {
    return {
      voyageno: this.Form1.controls['VoyageNo'].value,
      ownercode: this.Form1.controls['OwnerCode'].value,
      ownername: this.Form1.controls['OwnerName'].value,
      callsign: this.Form1.controls['CallSign'].value,
      shippingagentcode: this.Form1.controls['ShippingAgentCode'].value,
      shippingagent: this.Form1.controls['ShippingAgent'].value,
      shipmaster: this.Form1.controls['ShipMaster'].value,
      preferreddock: this.Form1.controls['PreferredDock'].value,
      berthtype: this.Form1.controls['BerthType'].value,
      portofcallname: this.PortCallName,
      portofcallcode: this.PortCallcode,
      nextportname: this.PortNextName,
      nextportcode: this.PortNextcode,
      etat: this.etat,
      type: this.type,
      previousportname: this.PortPreviousName,
      previousportcode: this.PortPreviouscode,
      callsigntype: this.CallSignType_f,
      eta: this.date_ETA,
      etd: this.date_ETD,
      dangerousgoods: this.DangerousGood,
      vessel: vessel,
    };
  }

  protected createFromVessel_Call_InfomationUpdate(
    vessel: any,
    etat: any
  ): any {
    return {
      id: this.Id_ArrCell,
      voyageno: this.Form1.controls['VoyageNo'].value,
      ownercode: this.Form1.controls['OwnerCode'].value,
      ownername: this.Form1.controls['OwnerName'].value,
      callsign: this.Form1.controls['CallSign'].value,
      shippingagentcode: this.Form1.controls['ShippingAgentCode'].value,
      shippingagent: this.Form1.controls['ShippingAgent'].value,
      shipmaster: this.Form1.controls['ShipMaster'].value,
      preferreddock: this.Form1.controls['PreferredDock'].value,
      berthtype: this.Form1.controls['BerthType'].value,
      portofcallname: this.PortCallName,
      portofcallcode: this.PortCallcode,
      nextportname: this.PortNextName,
      nextportcode: this.PortNextcode,
      previousportname: this.PortPreviousName,
      previousportcode: this.PortPreviouscode,
      callsigntype: this.CallSignType_f,
      eta: this.date_ETA,
      etd: this.date_ETD,
      etat: etat,
      type: this.type_f,
      dangerousgoods: this.DangerousGood,
      vessel: vessel,
    };
  }

  protected createFromVessel(): any {
    return {
      vesseldraft: this.Form1.controls['VesselDraft'].value,
      vesselgrossweight: this.Form1.controls['VesselGrossWeight'].value,
      vessellength: this.Form1.controls['VesselLength'].value,
      vessel: this.Form1.controls['VesselName'].value,
      vesselnetweight: this.Form1.controls['VesselNetWeight'].value,
      vesselregport: this.Form1.controls['VesselRegPort'].value,
      vesseltype: this.Form1.controls['VesselType'].value,
      vesselbuildyear: this.Form1.controls['VesselbuildYear'].value,
      vesselcountrycode: this.code_country,
      vesselcountryname: this.name_country,
    };
  }

  protected createFromVesselUpdate(id: any): any {
    return {
      id: id,
      vesseldraft: this.Form1.controls['VesselDraft'].value,
      vesselgrossweight: this.Form1.controls['VesselGrossWeight'].value,
      vessellength: this.Form1.controls['VesselLength'].value,
      vessel: this.Form1.controls['VesselName'].value,
      vesselnetweight: this.Form1.controls['VesselNetWeight'].value,
      vesselregport: this.Form1.controls['VesselRegPort'].value,
      vesseltype: this.Form1.controls['VesselType'].value,
      vesselbuildyear: this.Form1.controls['VesselbuildYear'].value,
      vesselcountrycode: this.code_country,
      vesselcountryname: this.name_country,
    };
  }

  protected createFromCargoSummary(i: any, call: any): any {
    return {
      cargoamount: this.CargoSummarylist[i].CargoAmount,
      cargocode: this.CargoSummarylist[i].CargoCode,
      operationcode: this.CargoSummarylist[i].OperationCode,
      arrVesselCallInformation: call,
    };
  }

  protected createFromVisitPurpose(i: any, call: any): any {
    return {
      reason: this.VisitPurposelist[i].reason,
      arrVesselCallInformation: call,
    };
  }

  protected createPassengerSummary(i: any, call: any): any {
    return {
      numberofpassenger: this.PassengerSummarylist[i].NumberofPassenger,
      passengernationally: this.PassengerSummarylist[i].PassengerNationally,
      arrVesselCallInformation: call,
    };
  }

<<<<<<< HEAD
  protected GetCellInformationById(id: any) {
    this.VesselCallApi.GetById(id).subscribe((res: any) => {
      this.shipping_arrival = res;
      this.code_country = res.vessel.vesselcountrycode;
      this.name_country = res.vessel.vesselcountryname;
      this.type_f = res.type;
=======
  type: any = 'Ship_Arrival';

  protected GetCellInformationById(id: any) {
    this.VesselCallApi.GetById(id).subscribe((res: any) => {
      this.code_country = res.vessel.vesselcountrycode;
      this.name_country = res.vessel.vesselcountryname;
      this.type_f = res.type;
      this.shipping_arrival = res
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
      this.Create_Form1_update(res, res.vessel);
      this.Create_Form2_update(res);
      this.Create_Form3_update(res);
      this.Create_Form4_update(res);
    });
  }
<<<<<<< HEAD

=======
  id_vessel: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  Create_Form1_update(Cell: any, Vessel: any) {
    //Cell
    this.Form1.controls['VoyageNo'].setValue(Cell.voyageno);
    this.Form1.controls['OwnerCode'].setValue(Cell.ownercode);
    this.Form1.controls['OwnerName'].setValue(Cell.ownername);
    //vessel
    this.Form1.controls['VesselName'].setValue(Vessel.vessel);
    this.Form1.controls['VesselType'].setValue(Vessel.vesseltype);
    this.Form1.controls['VesselbuildYear'].setValue(Vessel.vesselbuildyear);
    this.Form1.controls['VesselRegPort'].setValue(Vessel.vesselregport);
    this.Form1.controls['VesselLength'].setValue(Vessel.vessellength);
    this.Form1.controls['VesselGrossWeight'].setValue(Vessel.vesselgrossweight);
    this.Form1.controls['VesselNetWeight'].setValue(Vessel.vesselnetweight);
    this.Form1.controls['VesselDraft'].setValue(Vessel.vesseldraft);

    this.code_country = Vessel.vesselcountrycode;
    this.name_country = Vessel.vesselcountryname;
    this.id_vessel = Vessel.id;
  }

  Create_Form2_update(Cell: any) {
    if (Cell.callSignType == 'I') {
      this.CallSignType.setValue(this.options[0]);
    } else {
      this.CallSignType.setValue(this.options[1]);
    }

    this.dateETA.setValue(new Date(Cell.eta));
    this.dateETD.setValue(new Date(Cell.etd));

    this.Form1.controls['CallSign'].setValue(Cell.callsign);
    this.Form1.controls['ShippingAgentCode'].setValue(Cell.shippingagentcode);
    this.Form1.controls['ShippingAgent'].setValue(Cell.shippingagent);
    this.Form1.controls['ShipMaster'].setValue(Cell.shipmaster);
  }
<<<<<<< HEAD

=======
  port_next_u: any;
  port_perivious_u: any;
  port_ofcall_u: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  Create_Form3_update(Cell: any) {
    this.port_next_u = this.portlist.find((x) => x.code == Cell.nextportcode);
    this.port_perivious_u = this.portlist.find(
      (x) => x.code == Cell.previousportcode
    );
    this.port_ofcall_u = this.portlist.find(
      (x) => x.code == Cell.portofcallcode
    );

    this.Form1.controls['BerthType'].setValue(Cell.berthtype);
    this.Form1.controls['PreferredDock'].setValue(Cell.preferreddock);
    this.selectedportNext = this.port_next_u;
    this.selectedportcall = this.port_ofcall_u;
    this.selectedportPrevious = this.port_perivious_u;
    this.PortCallName = this.port_ofcall_u.nom;
    this.PortCallcode = this.port_ofcall_u.code;
    this.PortNextName = this.port_next_u.nom;
    this.PortNextcode = this.port_next_u.code;
    this.PortPreviousName = this.port_perivious_u.nom;
    this.PortPreviouscode = this.port_perivious_u.code;
  }
  Create_Form4_update(Cell: any) {
    for (let arrVciPassengerSummarys of Cell.arrVciPassengerSummarys) {
      this.PassengerSummarylist.push({
        id: arrVciPassengerSummarys.id,
        NumberofPassenger: arrVciPassengerSummarys.numberofpassenger,
        PassengerNationally: arrVciPassengerSummarys.passengernationally,
      });
    }

    for (let arrVciVisitPurposes of Cell.arrVciVisitPurposes) {
      this.VisitPurposelist.push({
        id: arrVciVisitPurposes.id,
        reason: arrVciVisitPurposes.reason,
      });
    }

    for (let arrVciCargoSummarys of Cell.arrVciCargoSummarys) {
      this.CargoSummarylist.push({
        id: arrVciCargoSummarys.id,
        CargoAmount: arrVciCargoSummarys.cargoamount,
        CargoCode: arrVciCargoSummarys.cargocode,
        OperationCode: arrVciCargoSummarys.cargoamount,
      });
    }
    this.CargoSummarydelete();
  }

  CargoSummarydelete() {
    for (let cargo of this.CargoSummarylist) {
      this.CargoSummaryApi.delete(cargo.id);
    }
  }
  VisitPurposesdelete() {
    for (let visit of this.VisitPurposelist) {
      this.VisitPurposeApi.delete(visit.id);
    }
  }
  PassengerSummarysdelete() {
    for (let passenger of this.PassengerSummarylist) {
      this.PassengerSummaryApi.delete(passenger.id);
    }
  }
<<<<<<< HEAD

=======
  form2 = 'form2';
  form1 = 'form1';
  selectionChange() {}
  Formbool1: boolean = true;
  Formbool2: boolean = false;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  ChangeForm(form: any) {
    if (form == 'form1') {
      this.Formbool1 = true;
      this.Formbool2 = false;
      this.btn_backround1 = 'rgba(71, 178, 228, 1)';
      this.btn_backround2 = '#fff';
      this.color1 = '#fff';
      this.color2 = '#073866';
      this.blockform_1 = 'block';
      this.blockform_2 = 'none';
    } else {
      this.blockform_1 = 'none';
      this.blockform_2 = 'block';
      this.form2Bool = true;
      this.Formbool1 = false;
      this.Formbool2 = true;
      this.btn_backround2 = 'rgba(71, 178, 228, 1)';
      this.btn_backround1 = '#fff';
      this.color2 = '#fff';
      this.color1 = '#073866';
    }
  }

<<<<<<< HEAD
=======
  contour: number = 1;

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  Next() {
    this.intialisebloc();
    if (this.contour < 4) {
      this.contour = this.contour + 1;
    }
    this.remlirbloc();
  }

  Preview() {
    if (this.contour > 1) {
      this.contour = this.contour - 1;
    }

    this.intialisebloc();
    this.remlirbloc();
  }
  intialisebloc() {
    this.BlocB1 = false;
    this.BlocB2 = false;
    this.BlocB3 = false;
    this.BlocB4 = false;
  }

  remlirbloc() {
    if (this.contour == 1) {
      this.BlocB1 = true;
    }
    if (this.contour == 2) {
      this.BlocB2 = true;
    }
    if (this.contour == 3) {
      this.BlocB3 = true;
    }
    if (this.contour == 4) {
      this.BlocB4 = true;
    }
  }

  changebloc(i: any) {
    this.contour = i;
    this.intialisebloc();
    this.remlirbloc();
  }

  Go_list() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('Logistic/liste/0');
  }

  Submit(content: any, content1: any) {
    this.etat = 'For confirmation';
<<<<<<< HEAD
    //   this.update_vessel(this.id_vessel, content);
=======
    this.update_vessel(this.id_vessel, content);
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e

    if (this.Form1.status != 'INVALID') {
      this.Vessel_Call_Infomation_add(content);
    } else {
      this.notifier.notify('error', 'Submit Opps !   Form Invalid !');
    }
  }
<<<<<<< HEAD
  Submit1() {
    this.VesselCallApi.submit(this.shipping_arrival).subscribe(
      (res: any) => {}
    );
  }

=======

  submit12()
  { console.log(this.shipping_arrival)
    this.shipping_arrival.etat = 'For confirmation';
    this.VesselCallApi.submit(this.shipping_arrival,2).subscribe(((res : any) =>
    {
      console.log(res)
    }
      ))
  }

  code_country_passenger: any;
  name_country_passenger: any;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  onCountrySelected_passenger(country: Country) {
    console.log(country.name);
    this.code_country_passenger = country.alpha2Code;
    this.name_country_passenger = country.name;
  }

<<<<<<< HEAD
=======
  y: any = 0;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  generate_pdf() {
    this.total_pass1 = 0;
    this.total_pass2 = 0;

    this.y = 0;
    var doc = new jsPDF();
    var img = new Image();
    img.src = './../../../../assets/logo_dark.png';
    doc.setFont('italic');
    doc.addImage(img, 'png', 150, 5, 50, 20);
    doc.setFontSize(10);
    doc.setTextColor('#46B1E3');
    doc.text('Voyage No :' + this.Form1.controls['VoyageNo'].value, 5, 10);
    doc.text('Shipping Company :' + '_', 5, 15);
    doc.setFontSize(20);
    doc.text('Shipping Arrival', 75, 15);
    doc.setFontSize(11);
    this.y = 40;
    let x1 = 10;
    let x2 = 60;
    let x3 = 110;
    let x4 = 150;
    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Vessel Call Information', 10, 40);

    doc.line(5, 43, 5, 65);
    doc.line(5, 43, 190, 43);
    doc.line(190, 43, 190, 65);
    doc.line(5, 65, 190, 65);

    doc.setTextColor('#1D3A66');
    this.y = 50;
    doc.setFontSize(9);

    doc.text(
      'Vessel Name : ' + this.Form1.controls['VesselName'].value,
      x1,
      this.y
    );
    doc.text(
      'Vessel Reg Port : ' + this.Form1.controls['VesselRegPort'].value,
      x2,
      this.y
    );
    doc.text(
      'Vessel Type : ' + this.Form1.controls['VesselType'].value,
      x3,
      this.y
    );
    doc.text(
      'Vessel Build Year : ' + this.Form1.controls['VesselbuildYear'].value,
      x4,
      this.y
    );

    doc.line(5, 73, 5, 105);
    doc.line(5, 73, 190, 73);
    doc.line(190, 73, 190, 105);
    doc.line(5, 105, 190, 105);
    this.y = 60;
    doc.setFontSize(9);
    doc.text(
      'Vessel Length : ' + this.Form1.controls['VesselLength'].value,
      x1,
      this.y
    );
    doc.text(
      'Vessel Gross weight : ' + this.Form1.controls['VesselGrossWeight'].value,
      x2,
      this.y
    );
    doc.text(
      'Vessel Net Weight : ' + this.Form1.controls['VesselNetWeight'].value,
      x3,
      this.y
    );
    doc.text(
      'Vessel Draft : ' + this.Form1.controls['VesselDraft'].value,
      x4,
      this.y
    );

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Ship Agent', 10, 70);
    doc.setTextColor('#1D3A66');

    this.y = 80;
    doc.setFontSize(9);
    doc.text(
      'Shipping Agent : ' + this.Form1.controls['ShippingAgent'].value,
      x1,
      this.y
    );
    doc.text(
      'Shipping Agent Code : ' + this.Form1.controls['ShippingAgentCode'].value,
      x2,
      this.y
    );
    doc.text(
      'Owner Name : ' + this.Form1.controls['OwnerName'].value + ' ',
      x3,
      this.y
    );
    doc.text(
      'Owner Code : ' + this.Form1.controls['OwnerCode'].value,
      x4,
      this.y
    );

    this.y = 90;
    doc.setFontSize(9);
    doc.text(
      'Ship Master : ' + this.Form1.controls['ShipMaster'].value,
      x1,
      this.y
    );
    doc.text('ETA : ' + this.date_ETA + ' ', x2, this.y);
    doc.text('ETD : ' + this.date_ETD, x3, this.y);
    doc.text(
      'Berth Type : ' + this.Form1.controls['BerthType'].value,
      x4,
      this.y
    );

    this.y = 100;
    doc.setFontSize(9);
    doc.text(
      'Preferred Dock : ' + this.Form1.controls['PreferredDock'].value,
      x1,
      this.y
    );
    doc.text('Country : ' + this.name_country + ' ', x2, this.y);
    doc.text(
      'Call Sign : ' + this.Form1.controls['CallSign'].value,
      x3,
      this.y
    );
    doc.text('Call Sign Type : ' + this.CallSignType_f, x4, this.y);

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Port', 10, 110);
    doc.setTextColor('#1D3A66');
    doc.line(5, 113, 5, 145);
    doc.line(5, 113, 190, 113);
    doc.line(190, 113, 190, 145);
    doc.line(5, 145, 190, 145);
    this.y = 120;
    doc.setFontSize(9);
    doc.text('Port Call Name : ' + this.PortCallName, x1, this.y);
    doc.text('Port Call Code : ' + this.PortCallcode + ' ', x2, this.y);

    this.y = 130;
    doc.setFontSize(9);
    doc.text('Port Previous Name : ' + this.PortPreviousName, x1, this.y);
    doc.text('Port Previous Code : ' + this.PortPreviouscode + ' ', x2, this.y);

    this.y = 140;
    doc.setFontSize(9);
    doc.text('Port Next Name : ' + this.PortNextName, x1, this.y);
    doc.text('Port Next Code : ' + this.PortNextcode, x2, this.y);

    doc.setFontSize(10);
    doc.setTextColor('#55EC7A');
    doc.text('Visit Purpose', 10, 150);
    doc.text('Cargo Summary', 10, 190);
    doc.text('Passenger Summary', 10, 230);

    doc.line(5, 155, 5, 185);
    doc.line(5, 155, 190, 155);
    doc.line(190, 155, 190, 185);
    doc.line(5, 185, 190, 185);

    doc.line(5, 195, 5, 225);
    doc.line(5, 195, 190, 195);
    doc.line(190, 195, 190, 225);
    doc.line(5, 225, 190, 225);

    doc.line(5, 235, 5, 280);
    doc.line(5, 235, 190, 235);
    doc.line(190, 235, 190, 280);
    doc.line(5, 280, 190, 280);
    let i = 0;
    let y = 0;
    let f = 0;
    doc.setFontSize(9);
    doc.setTextColor('#1D3A66');
    for (let cell1 of this.VisitPurposelist) {
      doc.text('reason : ' + cell1.reason + ' ', 15, 160 + i);
      i = i + 7;
    }
    for (let cell2 of this.CargoSummarylist) {
      doc.text('Cargo Code : ' + cell2.cargocode + ' ', 15, 200 + y);
      doc.text('Cargo Amount : ' + cell2.cargoamount + ' ', 60, 200 + y);
      doc.text('Operation Code : ' + cell2.operationcode + ' ', 100, 200 + y);
      y = y + 7;
    }
    for (let cell3 of this.PassengerSummarylist) {
      doc.text(
        'Number of Passenger : ' + cell3.numberofpassenger + ' ',
        15,
        250 + f
      );
      doc.text(
        'passengernationally  : ' + cell3.passengernationally + ' ',
        70,
        250 + f
      );

      if (cell3.passengernationally == 'Libya') {
        this.total_pass1 = this.total_pass1 + cell3.numberofpassenger;
      } else {
        this.total_pass2 = this.total_pass2 + cell3.numberofpassenger;
      }
      f = f + 7;
    }

    doc.text('Total Libyan Passenger : ' + this.total_pass1 + ' ', 15, 240);
    doc.text('Total foreigner Passenger  : ' + this.total_pass2 + ' ', 70, 240);

    doc.save(
      'SHipping_Arrival num' + this.Form1.controls['VoyageNo'].value + '.pdf'
    );
  }

<<<<<<< HEAD
=======
  total_pass1: any = 0;
  total_pass2: any = 0;

>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  generate_pdf2() {
    this.total_pass1 = 0;
    this.total_pass2 = 0;
    let cell = this.shipping_arrival;
    this.y = 0;
    var doc = new jsPDF();
    var img = new Image();
    img.src = './../../../../assets/logo_dark.png';
    doc.setFont('italic');
    doc.addImage(img, 'png', 150, 5, 50, 20);
    doc.setFontSize(10);
    doc.setTextColor('#46B1E3');
    doc.text('Voyage No :' + cell.voyageno, 5, 10);
    doc.text('Shipping Company :' + '_', 5, 15);
    doc.setFontSize(20);
    doc.text('Shipping Arrival', 75, 15);
    doc.setFontSize(11);
    this.y = 40;
    let x1 = 10;
    let x2 = 60;
    let x3 = 110;
    let x4 = 150;
    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Vessel Call Information', 10, 40);

    doc.line(5, 43, 5, 65);
    doc.line(5, 43, 190, 43);
    doc.line(190, 43, 190, 65);
    doc.line(5, 65, 190, 65);

    doc.setTextColor('#1D3A66');
    this.y = 50;
    doc.setFontSize(9);

    doc.text('Vessel Name : ' + cell.vessel.vessel, x1, this.y);
    doc.text('Vessel Reg Port : ' + cell.vessel.vesselregport, x2, this.y);
    doc.text('Vessel Type : ' + cell.vessel.vesseltype, x3, this.y);
    doc.text('Vessel Build Year : ' + cell.vessel.vesselbuildyear, x4, this.y);

    doc.line(5, 73, 5, 105);
    doc.line(5, 73, 190, 73);
    doc.line(190, 73, 190, 105);
    doc.line(5, 105, 190, 105);
    this.y = 60;
    doc.setFontSize(9);
    doc.text('Vessel Length : ' + cell.vessel.vessellength, x1, this.y);
    doc.text(
      'Vessel Gross weight : ' + cell.vessel.vesselgrossweight,
      x2,
      this.y
    );
    doc.text('Vessel Net Weight : ' + cell.vessel.vesselnetweight, x3, this.y);
    doc.text('Vessel Draft : ' + cell.vessel.vesseldraft, x4, this.y);

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Ship Agent', 10, 70);
    doc.setTextColor('#1D3A66');

    this.y = 80;
    doc.setFontSize(9);
    doc.text('Shipping Agent : ' + cell.shippingagent, x1, this.y);
    doc.text('Shipping Agent Code : ' + cell.shippingagentcode, x2, this.y);
    doc.text('Owner Name : ' + cell.vessel.ownername + ' ', x3, this.y);
    doc.text('Owner Code : ' + cell.ownercode, x4, this.y);

    this.y = 90;
    doc.setFontSize(9);
    doc.text('Ship Master : ' + cell.shipmaster, x1, this.y);
    doc.text('ETA : ' + cell.eta + ' ', x2, this.y);
    doc.text('ETD : ' + cell.etd, x3, this.y);
    doc.text('Berth Type : ' + cell.berthtype, x4, this.y);

    this.y = 100;
    doc.setFontSize(9);
    doc.text('Preferred Dock : ' + cell.preferreddock, x1, this.y);
    doc.text('Country : ' + cell.vessel.vesselcountryname + ' ', x2, this.y);
    doc.text('Call Sign : ' + cell.callsign, x3, this.y);
    doc.text('Call Sign Type : ' + cell.callsigntype, x4, this.y);

    doc.setFontSize(11);
    doc.setTextColor('#55EC7A');
    doc.text('Port', 10, 110);
    doc.setTextColor('#1D3A66');
    doc.line(5, 113, 5, 145);
    doc.line(5, 113, 190, 113);
    doc.line(190, 113, 190, 145);
    doc.line(5, 145, 190, 145);
    this.y = 120;
    doc.setFontSize(9);
    doc.text('Port Call Name : ' + cell.portofcallname, x1, this.y);
    doc.text('Port Call Code : ' + cell.portofcallcode + ' ', x2, this.y);

    this.y = 130;
    doc.setFontSize(9);
    doc.text('Port Previous Name : ' + cell.previousportname, x1, this.y);
    doc.text('Port Previous Code : ' + cell.previousportcode + ' ', x2, this.y);

    this.y = 140;
    doc.setFontSize(9);
    doc.text('Port Next Name : ' + cell.nextportname, x1, this.y);
    doc.text('Port Next Code : ' + cell.nextportcode, x2, this.y);

    doc.setFontSize(10);
    doc.setTextColor('#55EC7A');
    doc.text('Visit Purpose', 10, 150);
    doc.text('Cargo Summary', 10, 190);
    doc.text('Passenger Summary', 10, 230);

    doc.line(5, 155, 5, 185);
    doc.line(5, 155, 190, 155);
    doc.line(190, 155, 190, 185);
    doc.line(5, 185, 190, 185);

    doc.line(5, 195, 5, 225);
    doc.line(5, 195, 190, 195);
    doc.line(190, 195, 190, 225);
    doc.line(5, 225, 190, 225);

    doc.line(5, 235, 5, 280);
    doc.line(5, 235, 190, 235);
    doc.line(190, 235, 190, 280);
    doc.line(5, 280, 190, 280);
    let i = 0;
    let y = 0;
    let f = 0;
    doc.setFontSize(9);
    doc.setTextColor('#1D3A66');
    for (let cell1 of this.VisitPurposelist) {
      doc.text('reason : ' + cell1.reason + ' ', 15, 160 + i);
      i = i + 7;
    }

    for (let cell2 of this.CargoSummarylist) {
      doc.text('Cargo Code : ' + cell2.CargoCode + ' ', 15, 200 + y);
      doc.text('Cargo Amount : ' + cell2.CargoAmount + ' ', 60, 200 + y);
      doc.text('Operation Code : ' + cell2.OperationCode + ' ', 100, 200 + y);
      y = y + 7;
    }
    for (let cell3 of this.PassengerSummarylist) {
      doc.text(
        'Number of Passenger : ' + cell3.NumberofPassenger + ' ',
        15,
        250 + f
      );
      doc.text(
        'passengernationally  : ' + cell3.PassengerNationally + ' ',
        70,
        250 + f
      );

      if (cell3.PassengerNationally == 'Libya') {
        this.total_pass1 = this.total_pass1 + cell3.NumberofPassenger;
      } else {
        this.total_pass2 = this.total_pass2 + cell3.NumberofPassenger;
      }
      f = f + 7;
    }

    doc.text('Total Libyan Passenger : ' + this.total_pass1 + ' ', 15, 240);
    doc.text('Total foreigner Passenger  : ' + this.total_pass2 + ' ', 70, 240);

    doc.save('SHipping_Arrival num' + cell.voyageno + '.pdf');
  }
<<<<<<< HEAD

  onFileChange(event: any) {
    this.spinner.show();
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>event.target;
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      setTimeout(() => {
        this.Form1.controls['VoyageNo'].setValue(ws['C9']['w']);
        this.Form1.controls['VesselName'].setValue(ws['C40']['v']);
        this.Form1.controls['VesselType'].setValue(ws['E40']['w']);
        this.Form1.controls['OwnerCode'].setValue(ws['G40']['w']);
        this.Form1.controls['CallSign'].setValue(ws['C41']['v']);
        this.Form1.controls['PreferredDock'].setValue(ws['E41']['w']);
        this.Form1.controls['ShipMaster'].setValue(ws['G41']['w']);

        this.dateETA.setValue(new Date(ws['E13']['w']));
        this.dateETD.setValue(new Date(ws['G13']['w']));

        ///port
        this.port_next_u = this.portlist.find((x) => x.nom == ws['F30']['v']);
        this.port_perivious_u = this.portlist.find(
          (x) => x.nom == ws['F22']['v']
        );
        this.port_ofcall_u = this.portlist.find((x) => x.nom == ws['D20']['v']);
        this.selectedportNext = this.port_next_u;
        this.selectedportcall = this.port_ofcall_u;
        this.selectedportPrevious = this.port_perivious_u;
        this.PortCallName = this.port_ofcall_u.nom;
        this.PortCallcode = this.port_ofcall_u.code;
        this.PortNextName = this.port_next_u.nom;
        this.PortNextcode = this.port_next_u.code;
        this.PortPreviousName = this.port_perivious_u.nom;
        this.PortPreviouscode = this.port_perivious_u.code;

        this.Form1.controls['VesselLength'].setValue(Number(ws['C18']['v']));
        this.Form1.controls['VesselbuildYear'].setValue(Number(ws['F18']['v']));
        this.Form1.controls['VesselDraft'].setValue(Number(ws['H18']['v']));
        this.Form1.controls['VesselNetWeight'].setValue(Number(ws['F19']['v']));
        this.Form1.controls['VesselGrossWeight'].setValue(
          Number(ws['H19']['v'])
        );
        this.spinner.hide();
      }, 500);
      /* save data */
      const data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    };
=======
}

export class port {
  id: number | undefined;
  name: String | undefined;
  nom: String | undefined;
  code: String | undefined;

  constructor(id: number, name: string, nom: string, code: string) {
    this.id = id;
    this.name = name;
    this.nom = nom;
    this.code = code;
>>>>>>> 9d09482dd6d8487ab459aa0b9b8f460c7b4d357e
  }
}
