import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, UntypedFormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CargoSummaryService } from '../Service/CargoSummary/cargo-summary.service';
import { PassengerSummaryService } from '../Service/PassengerSummary/passenger-summary.service';
import { VesselCallInformationService } from '../Service/VesselCall/vessel-call-information.service';
import { VisitPurposeService } from '../Service/VisitPurpose/visit-purpose.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { SelectionModel } from '@angular/cdk/collections';
import { jsPDF } from 'jspdf';
import { DatePipe } from '@angular/common';
export interface Cell {
  id: string;
  voyageno: string;
  vessel: string;
  port_declaration: string;
  eta: string;
  callsign: string;
  port_previous: string;
}
@Component({
  selector: 'app-liste_depart',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css'],
})
export class ListeComponent implements AfterViewInit {
  index: number = 1;
  displayedColumns: string[] = [
    'select',
    'name',
    'progress',
    'portd',
    'eta',
    'callsign',
    'previous',
    'etat',
    'action',
  ];
  dataSource: MatTableDataSource<Cell>;
  itemlist: any[] = ['ETA', 'ETD'];
  statuslist: any[] = ['Save', 'For confirmation', 'Reject', 'Accept'];
  itemslist: any[] = ['Call Sign', 'Port'];
  selectedtype: String | any;
  selectedstatus: String | any;
  selectedcondition: String | any;
  CellList: Cell[] = [];
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  vesselname: any;
  vesseltype: any;
  vesselbuildyear: any;
  vesselregport: any;
  vesseldraft: any;
  vessellength: any;
  vesselgrossweight: any;
  vesselnetweight: any;
  voyageno: any;
  callsigntype: any;
  callsign: any;
  ownercode: any;
  ownername: any;
  shippingagentcode: any;
  shippingagent: any;
  shipmaster: any;
  eta: any;
  etd: any;
  previousportcode: any;
  previousportname: any;
  portofcallcode: any;
  portofcallname: any;
  nextportcode: any;
  nextportname: any;
  preferreddock: any;
  berthtype: any;
  dangerousgoods: any;
  constructor(
    private ArrCellInformationApi: VesselCallInformationService,
    private http: HttpClient,
    private VisitPurposeApi: VisitPurposeService,
    private CargoSummaryApi: CargoSummaryService,
    private PassengerSummaryApi: PassengerSummaryService,
    private modalService: NgbModal,
    private datePipe: DatePipe
  ) {
    this.dataSource = new MatTableDataSource(this.CellList);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.GetArrCellInformation();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  list_delete: any[] = [];
  daterange = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  onchangedatestart(event: any) {
    this.startdate = this.datePipe.transform(event, 'yyyy-MM-dd');
  }
  startdate: any;
  enddate: any;
  listrecherchr: any;
  onchangedateend(event: any) {
    this.CellList = [];
    this.enddate = this.datePipe.transform(event, 'yyyy-MM-dd');

    if (this.typerecherche == 'ETA') {
      this.listrecherchr = this.CellList_1.filter(
        (e) => e.eta > this.startdate && e.eta < this.enddate
      );
      for (let cell of this.listrecherchr) {
        let nom_vessel = '_';
        if (cell.vessel != null) {
          nom_vessel = cell.vessel.vessel;
        }

        let c = this.createnewCell(cell.id, cell, nom_vessel);
        this.CellList.push(c);
      }
      setTimeout(() => {
        this.dataSource = new MatTableDataSource(this.CellList);
        this.dataSource.paginator = this.paginator;
      }, 100);
    } else if (this.typerecherche == 'ETD') {
      this.listrecherchr = this.CellList_1.filter(
        (e) => e.etd > this.startdate && e.etd < this.enddate
      );
      for (let cell of this.listrecherchr) {
        let nom_vessel = '_';
        if (cell.vessel != null) {
          nom_vessel = cell.vessel.vessel;
        }

        let c = this.createnewCell(cell.id, cell, nom_vessel);
        this.CellList.push(c);
      }
      setTimeout(() => {
        this.dataSource = new MatTableDataSource(this.CellList);
        this.dataSource.paginator = this.paginator;
      }, 100);
    } else {
      this.GetArrCellInformation();
    }
  }
  Arr_remove() {
    this.list_delete = [];
    this.selection.selected.forEach((s) => this.list_delete.push(s));

    for (let cell of this.list_delete) {
      let cel = this.CellList_1.find((x) => x.id == cell.id);

      for (let cargo of cel.arrVciCargoSummarys) {
        this.CargoSummary_Delete(cargo.id);
      }

      for (let passenger of cel.arrVciPassengerSummarys) {
        this.PassengerSummary_Delete(passenger.id);
      }

      for (let visit of cel.arrVciVisitPurposes) {
        this.VisitPurpose_Delete(visit.id);
      }

      setTimeout(() => {
        this.ArrCellInformationApi.delete(cel.id).subscribe((res: any) => {
          setTimeout(() => {
            this.GetArrCellInformation();
          }, 200);

          this.modalService.dismissAll();
        });
      }, 500);
    }
  }
  Arr_view(content: any, id: any) {
    this.ArrCellInformationApi.GetById(id).subscribe((res: any) => {
      this.Create_form(res);
      setTimeout(() => {
        this.modalService.open(content, {
          size: 'xl',
          windowClass: 'dark-modal',
        });
      }, 500);
    });
  }

  open(content: any) {
    this.modalService.open(content);
  }
  Arr_update() {}
  CellList_1: any[] = [];

  Create_form(call: any) {
    let vessel = call.vessel;
    this.vesselname = vessel.vessel;
    this.vesseltype = vessel.vesseltype;
    this.vesselbuildyear = vessel.vesselbuildyear;
    this.vesselregport = vessel.vesselregport;
    this.vesseldraft = vessel.vesseldraft;
    this.vessellength = vessel.vessellength;
    this.vesselgrossweight = vessel.vesselgrossweight;
    this.vesselnetweight = vessel.vesselnetweight;
    this.voyageno = vessel.voyageno;

    this.voyageno = call.voyageno;
    this.callsigntype = call.callsigntype;
    this.callsign = call.callsign;
    this.ownercode = call.ownercode;
    this.ownername = call.ownername;
    this.shippingagentcode = call.shippingagentcode;
    this.shippingagent = call.shippingagent;
    this.shipmaster = call.shipmaster;
    this.eta = call.eta;
    this.etd = call.etd;
    this.previousportcode = call.previousportcode;
    this.previousportname = call.previousportname;
    this.portofcallcode = call.portofcallcode;
    this.portofcallname = call.portofcallname;
    this.nextportcode = call.nextportcode;
    this.nextportname = call.nextportname;
    this.preferreddock = call.preferreddock;
    this.berthtype = call.berthtype;
    this.dangerousgoods = call.dangerousgoods;

    this.CargoSummarysList = call.arrVciCargoSummarys;
    this.PassengerSummarysList = call.arrVciPassengerSummarys;
    this.VisitPurposesList = call.arrVciVisitPurposes;
  }
  GetArrCellInformation() {
    this.CellList = [];
    this.ArrCellInformationApi.GetByType('Ship_Departure').subscribe(
      (res: any) => {
        this.CellList_1 = res;

        //this.Create_form(res[0]);
        for (let cell of res) {
          let nom_vessel = '_';
          if (cell.vessel != null) {
            nom_vessel = cell.vessel.vessel;
          }

          let c = this.createnewCell(cell.id, cell, nom_vessel);
          this.CellList.push(c);
        }
        setTimeout(() => {
          this.dataSource = new MatTableDataSource(this.CellList);
          this.dataSource.paginator = this.paginator;
        }, 100);
      }
    );
  }

  createnewCell(id: any, cell: any, vessel: any) {
    return {
      id: id,
      voyageno: cell.voyageno,
      vessel: vessel,
      port_declaration: cell.portofcallcode,
      eta: cell.eta,
      callsign: cell.callsign,
      port_previous: cell.nextportcode,
      etat: cell.etat,
    };
  }

  Save: any = 'Save';
  ngOnInit(): void {}
  typerecherche: any;
  changestatus(event: any) {
    if (event == undefined) {
      this.GetArrCellInformation();
    }
    this.CellList = [];

    this.listrecherchr = this.CellList_1.filter((e) => e.etat == event);
    for (let cell of this.listrecherchr) {
      let nom_vessel = '_';
      if (cell.vessel != null) {
        nom_vessel = cell.vessel.vessel;
      }

      let c = this.createnewCell(cell.id, cell, nom_vessel);
      this.CellList.push(c);
    }
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.CellList);
      this.dataSource.paginator = this.paginator;
    }, 100);
  }
  changeconditionitem(event: any) {}
  changetype(event: any) {
    this.typerecherche = event;
  }

  CargoSummarysList: any[] = [];
  PassengerSummarysList: any[] = [];
  VisitPurposesList: any[] = [];
  VisitPurpose_Delete(id: any) {
    this.VisitPurposeApi.delete(id).subscribe((res: any) => {
      console.log(res);
    });
  }

  CargoSummary_Delete(id: any) {
    this.CargoSummaryApi.delete(id).subscribe((res: any) => {
      console.log(res);
    });
  }
  PassengerSummary_Delete(id: any) {
    this.PassengerSummaryApi.delete(id).subscribe((res: any) => {
      console.log(res);
    });
  }

  selection = new SelectionModel<any>(true, []);
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  list_print: any[] = [];
  printdoc() {
    this.list_print = [];
    this.selection.selected.forEach((s) => this.list_print.push(s));

    for (let cell of this.list_print) {
      let cel = this.CellList_1.find((x) => x.id == cell.id);
      this.generate_pdf(cel);
    }
  }

  generate_pdf(cell: any) {
    let table: any[] = [];
    let table1: any[] = [];
    let table2: any[] = [];
    let table3: any[] = [];
    let table4: any[] = [];
    let table5: any[] = [];
    let table6: any[] = [];
    let table7: any[] = [];

    table.push({
      Vessel_Type: cell.vessel.vesseltype + '    ',
      Vessel_Name: cell.vessel.vessel + '    ',
      Vessel_BuildYear: cell.vessel.vesselbuildyear + '    ',
      Vessel_RegPort: cell.vessel.vesselregport + '    ',
    });
    table1.push({
      VesselLength: cell.vessel.vessellength + '    ',
      VesselGrossWeight: cell.vessel.vesselgrossweight + '    ',
      VesselNetWeight: cell.vessel.vesselnetweight + '    ',
      VesselDraft: cell.vessel.vesseldraft + '    ',
    });

    table2.push({
      Owner_Code: cell.ownercode + '    ',
      Owner_Name: cell.ownername + '    ',
      Call_Sign: cell.callsign + '    ',
      Shipping_AgentCode: cell.shippingagentcode + '    ',
    });
    table3.push({
      Ship_Master: cell.shipmaster + '    ',
      ETA: cell.eta + '    ',
      ETD: cell.etd + '    ',
      Berth_Type: cell.berthtype + '    ',
    });
    table7.push({
      Preferred_Dock: cell.preferreddock + '    ',
    });
    table4.push({
      Dangerous_Good: cell.dangerousgoods + '    ',
      Country: '_' + '',
      Call_SignType: cell.callsigntype + '    ',
      Shipping_Agent: cell.shippingagent + '    ',
    });
    table5.push({
      Port_CallName: cell.portofcallname + '    ',
      Port_CallCode: cell.portofcallcode + '    ',
      Port_NextName: cell.nextportname + '    ',
      Port_NextCode: cell.nextportcode + '    ',
    });
    table6.push({
      Port_PreviousName: cell.previousportname + '    ',
      Port_PreviousCode: cell.previousportcode + '    ',
    });

    var doc = new jsPDF();
    var img = new Image();
    img.src = './../../../../assets/logo_dark.png';

    doc.addImage(img, 'png', 150, 5, 50, 20);

    doc.text('Voyage No :' + cell.voyageno, 20, 30);
    /*   doc.text('Vessel', 15, 50);
    doc.text('Port', 20, 230); */
    doc.setFont('Lato-Regular', 'Courier-Bold');
    doc.table(10, 50, table, this.headers, { fontSize: 9 });
    doc.table(10, 80, table1, this.headers1, { fontSize: 8 });
    doc.table(10, 110, table2, this.headers2, { fontSize: 9 });
    doc.table(10, 140, table3, this.headers3, { fontSize: 8 });
    doc.table(10, 170, table4, this.headers4, { fontSize: 8 });
    doc.table(10, 200, table7, this.headers7, { fontSize: 8 });
    doc.table(10, 230, table5, this.headers5, { fontSize: 8 });
    doc.table(10, 260, table6, this.headers6, { fontSize: 8 });

    doc.save('num' + cell.voyageno + '.pdf'); //Download the rendered PDF.
  }
  headers = [
    'Vessel_Name',
    'Vessel_Type',
    'Vessel_RegPort',
    'Vessel_BuildYear',
  ];
  headers1 = [
    'VesselLength',
    'VesselGrossWeight',
    'VesselNetWeight',
    'VesselDraft',
  ];
  headers2 = ['Owner_Code', 'Owner_Name', 'Call_Sign', 'Shipping_AgentCode'];
  headers3 = ['Ship_Master', 'ETA', 'ETD', 'Berth_Type'];
  headers4 = ['Dangerous_Good', 'Country', 'Call_SignType', 'Shipping_Agent'];
  headers7 = ['Preferred_Dock'];
  headers5 = [
    'Port_CallName',
    'Port_CallCode',
    'Port_NextName',
    'Port_NextCode',
  ];
  headers6 = ['Port_PreviousName', 'Port_PreviousCode'];

  dimision() {
    this.modalService.dismissAll();
  }
}
