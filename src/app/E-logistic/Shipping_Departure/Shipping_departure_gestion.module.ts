import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ArchwizardModule } from 'angular-archwizard';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatTabsModule } from '@angular/material/tabs';

import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

import { ListeComponent } from './liste/liste.component';
import { NewComponent } from './new/new.component';

@NgModule({
  declarations: [NewComponent, ListeComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatDividerModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    ArchwizardModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    NgSelectModule,
    MatStepperModule,
    MatRadioModule,
    BrowserAnimationsModule,
    MatCardModule,
    NgxSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    BsDropdownModule.forRoot(),
    MatSelectCountryModule.forRoot('en'),
    RouterModule.forChild([
      {
        path: 'ShippingDeparture/new',
        component: NewComponent,
      },

      {
        path: 'ShippingDeparture/update/:id',
        component: NewComponent,
      },
      {
        path: 'ShippingDeparture/liste',
        component: ListeComponent,
      },
    ]),
  ],
  exports: [ListeComponent],
  providers: [DatePipe],
})
export class Shipping_departure_gestionModule {}
