import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class VesselCallInformationService {
  url: any;

  Spring_serveur: string = 'api/ArrVesselCallInformation/';
  constructor(private router: Router, private http: HttpClient) {
    this.url = environment.Url + this.Spring_serveur;
  }
  public Get() {
    return this.http.get<any>(`${this.url + 'List'}`);
  }
  public GetById(id: any) {
    return this.http.get<any>(`${this.url + 'ArrVesselInfo/' + id + '/'}`);
  }

  public GetByType(type: any) {
    return this.http.get<any>(
      `${this.url + 'ArrVesselCallInformationtype?type=' + type}`
    );
  }

  public post(formData: any) {
    return this.http.post(this.url + 'save', formData, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }
  public update(formData: any) {
    return this.http.put<any>(`${this.url + 'update/'}`, formData);
  }
  public delete(id: any) {
    return this.http.delete<any>(
      `${this.url + 'ArrVesselCallInformations' + '/' + id + '/'}`
    );
  }
}
