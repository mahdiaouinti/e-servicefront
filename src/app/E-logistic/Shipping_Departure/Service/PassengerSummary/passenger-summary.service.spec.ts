import { TestBed } from '@angular/core/testing';

import { PassengerSummaryService } from './passenger-summary.service';

describe('PassengerSummaryService', () => {
  let service: PassengerSummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PassengerSummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
