import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MessageUserService {
  url: any;

  Spring_serveur: string = 'api/';
  constructor(private http: HttpClient) {
    this.url = environment.Url + this.Spring_serveur;
  }
  public Get(id: any) {
    return this.http.get<any>(`${this.url + 'message-usersbyship/' + id}`);
  }
}
