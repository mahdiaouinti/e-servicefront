import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor() {}

  getDecodedAccessToken(token: any): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  IsUserLogged() {
    if (localStorage.getItem('token') != undefined) {
      return true;
    } else {
      return false;
    }
  }

  Validtoken(): boolean {
    let token = localStorage.getItem('token');
    let generadtoken = this.getDecodedAccessToken(token);

    const date1 = moment();
    let todayDate = date1.format('YYYY-MM-DD HH:mm:ss');

    const dateexpired = moment(generadtoken['exp'] * 1000).format(
      'YYYY-MM-DD HH:mm:ss'
    );

    if (dateexpired > todayDate) {
      return true;
    } else {
      return false;
    }
  }

  GetUsername() {
    let token = localStorage.getItem('token');
    let generadtoken = this.getDecodedAccessToken(token);
    return generadtoken['sub'];
  }
}
